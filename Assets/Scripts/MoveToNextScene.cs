﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveToNextScene : MonoBehaviour {

    public float loadTime = .5f;


	void Start () {
        Invoke("LoadScene", loadTime);

    }

    private void LoadScene() {
        SceneManager.LoadScene(1);
    }
}
