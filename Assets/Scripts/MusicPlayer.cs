﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    public bool mute = true;

    [Header("Music Clips")]
    public AudioClip[] songs;

    private AudioSource audio;


    void Awake () {
        if(!mute) {
            audio = GetComponent<AudioSource>();

            DontDestroyOnLoad(this);
        }
	}

    private void OnLevelWasLoaded(int level)
    {
        ChangeSong(level);
    }

    public void ChangeSong(int songID) {
        audio.clip = songs[songID - 1];
        audio.loop = true;
        audio.Play();

        Debug.Log("Song playing: " + audio.clip.name);
    }
}
