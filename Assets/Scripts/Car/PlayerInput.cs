﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerInput : MonoBehaviour {

    public enum InputType {
        VirtualJoystick,
        OnScreenButtons,
        Gamepad
    }

    public float inputX;
    public float inputY;
    public bool throttle = false;
    public bool brake = false;

    public GameObject virtualJoystick;
    public GameObject onScreenButtons;

    public InputType inputType = InputType.VirtualJoystick;

    private CarAudioController carAudio;


	void Start () {
        carAudio = GetComponent<CarAudioController>();

        CheckControlScheme();
    }

    private void Update()
    {
        //When the user has chosen virtual joystick as input scheme
        if(inputType == InputType.VirtualJoystick) {
            inputX = CrossPlatformInputManager.GetAxis("Horizontal");
            inputY = CrossPlatformInputManager.GetAxis("Vertical");
        }
    }
 
    public void OnScreenButton_TurnLeft(bool released) {
        if(!released) {
            inputX = -1;
        }
        else {
            inputX = 0;
        }
    }

    public void OnScreenButton_TurnRight(bool released) {
        if(!released) {
            inputX = 1;
        }
        else {
            inputX = 0;
        }
    }

    public void Throttle(bool released) {
        if(!released) {
            throttle = true;
            //carAudio.SelectClip();
        }
        else {
            throttle = false;
            //carAudio.SelectClip();
        }
    }

    public void Brake(bool released) {
        if(!released) {
            brake = true;
        }
        else {
            brake = false;
        }
    }

    protected void CheckControlScheme() {
        if(inputType == InputType.VirtualJoystick) {
            virtualJoystick.SetActive(true);
            onScreenButtons.SetActive(false);
        }
        else if(inputType == InputType.OnScreenButtons) {
            virtualJoystick.SetActive(false);
            onScreenButtons.SetActive(true);
        }
        else if(inputType == InputType.Gamepad) {
            virtualJoystick.SetActive(false);
            onScreenButtons.SetActive(false);
        }
    }
}
