﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AiCarEngine : MonoBehaviour {

    // ToDo Notelist
    // 1. Even with z rot frozen, the car may start tilting from side to side a bit so 2 of the wheels wont touch the ground
    //      This is propably because the collider is in mesh object instead of the car itself, cant put mesh collider as it has wrong rotation
    // 2. Reverse logic still jacks up sometimes. Needs further investigation
    // 3. Can some more update functions be run in invokes?
    // 4. The sensor rays could adjust with the speed?
    // 5. The car is still lagging/jittering, check movement function and/or the collisions happening between car and wheels
    // 6. The wheels rotation was disabled since the wheel script also caused the wheels to lag behind
    // 7. Going right way check still needs a foolproofing, it can start showing "right way" if one goes wrongway for half a map
    //      Could make system to get "lastNodeVisited" and make system compare lastNode-NextNode, instead of current one with
    //          lastNodeProcced-nextNode. 

    [Header("Debugging Tools")]
    public bool debuggingOn;
    [Header("Ai Car Vars")]
    public Transform path;
    public Vector3 centerOfMass;
    [Range(0, 180)] public float maxSteerAngle = 45f;
    [Range(0, 2)] public float steerSpeed = 0.25f;
    [Range(0,3)] public int steeringVersion = 1;
    [Range(0, 2500)] public float enginePower = 1000f;
    [Range(0, 2500)] public float maxTorque = 1000f;
    [Range(50, 500)] public float downforce = 340;
    public float curSpeed = 20f;
    [Range(0, 250)] public float topSpeed = 150f;
    [Range(0, 250)] public float minSpeed = 10f;
    [Range(0, 100)] public float deaccelerationSpeed = 10f;
    [Range(0, 500)] public float maxBrakeTorgue = 20f;
    [Range(0, 2)] public float brakeSpeed = 0.5f;
    [Range(0, 1)] public float tractionControl;
    [Range(10, 100)] public float slipLimit = 50;
    [Range(0, 100)] public float distFromNode = 3f;
    [Range(0, 20)] public float unstuckTime = 5f;
    private float unstuckTimer;
    public WheelCollider[] wheels;
    public GameObject[] wheelMeshes;

    private List<Transform> nodes;
    private int curNode = 0;

    [Header ("Braking Light")]
    public Renderer brakingLights;
    public Material idleBrakeLight;
    public Material activeBrakeLight;
    public bool isBraking;

    [Header("Sensors")]
    public LayerMask layermask;
    [Range(0, 50)] public float sensorLenght = 5f;
    [Range(0, 50)] public float frontSensorStartPoint = 5f;
    [Range(0, 50)] public float frontSensorSideDist = 5f;
    [Range(0, 90)] public float frontSensorAngle = 30f;
    [Range(0, 50)] public float sidewaySensorLenght = 5;
    [Range(0, 100)] public float avoidSpeed = 10f;
    private bool detected;

    public bool goingRightWay;
    [Range(0, 180)] public float angleOfRightWayDetection = 90f;
    private float startAngleOfRightwayDetection;

    public bool reversing;
    public bool wwReversing;
    [Range(0, 5)] public float waitToReverse = 1f;
    [Range(0, 5)] public float reverseForMax = 2f;
    public bool inReverseRcast;

    public LapTimeSystem intervalManager;

    private float curTorque;
    private float curTime;
    private float curBrakeTime;
    private Rigidbody rb;

    void Start ()
    {
        Setup();
        GetPath();

        InvokeRepeating("CheckDirection", 0f, 1f);
    }
    #region Get Path
    void GetPath ()
    {
        if (path == null)
            path = GameObject.Find("PathV2").GetComponent<Transform>();
        // Get the path transforms
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
    }
    #endregion
    void FixedUpdate ()
    {
        // If theres no obstacles detected by the sensors, apply normal steering
        if(!detected)
            ApplySteer();

        Movement();
        ApplyDownForce();
        TractionControl();
        BrakingEffect();
        Sensors();
        UnstuckSystem();
        AlignWheels();

        //DebugKeys();
    }

    void DebugKeys()
    {
        if(Input.GetKeyDown("space"))
        {
            Debug.Log("Ding");
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;
        }
    }

    void ApplySteer ()
    {
        // Get the relative vector between car and the waypoint (instead of 0,0)
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[curNode].position);
        //relativeVector /= relativeVector.magnitude;

        // Calculate and apply steering to the wheel colliders
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;

        //Unlerped steering V1
        if (steeringVersion == 1)
        {
            wheels[0].steerAngle = newSteer;
            wheels[1].steerAngle = newSteer;
        }

        //Lerped steering V2
        if (steeringVersion == 2)
        {
            if (curTime <= steerSpeed)
            {
                curTime += Time.deltaTime;
                wheels[0].steerAngle = Mathf.Lerp(wheels[0].steerAngle, newSteer, curTime / steerSpeed);
                wheels[1].steerAngle = Mathf.Lerp(wheels[1].steerAngle, newSteer, curTime / steerSpeed);
            }
            else
            {
                newSteer = wheels[0].steerAngle;
                curTime = 0f;
            }
        }

        // Set the next node to go to when reached current one
        if (relativeVector.magnitude <= distFromNode)
        {
            if (curNode < nodes.Count - 1)
                curNode++;
            else
                curNode = 0;
        }
    }

    // Apply throttle
    void Movement ()
    {
        // Current speed being calculated from RPM and rounded
   /*     curSpeed = 2 * Mathf.PI * wheelFL.radius*((wheelFL.rpm + wheelFR.rpm)*0.5f)*60/1000; // Temp solution to take curSpeed from rbs velocity currently active, remove these comms and curspeed = curspeed2 to go back to original
        curSpeed = Mathf.Round(curSpeed);*/
        // Cur speed calculated by the rigidbody
        curSpeed = rb.velocity.magnitude;

        // While hasnt topped maxSpeed and is not braking, accelerate with enginePower
        if (curSpeed <= topSpeed && !isBraking)
        {
            // If not reversing, accelerate
            if (!reversing)
            {
                wheels[0].motorTorque = enginePower;
                wheels[1].motorTorque = enginePower;
                wheels[2].motorTorque = enginePower;
                wheels[3].motorTorque = enginePower;
            }
            // Else if reversing, apply reverse as negative enginepower
            else
            {
                wheels[0].motorTorque = -enginePower;
                wheels[1].motorTorque = -enginePower;
                wheels[2].motorTorque = -enginePower;
                wheels[3].motorTorque = -enginePower;

                if (debuggingOn)
                    Debug.Log("Reversing");
            }
            
            // Make sure theres not brakeTorque applied when accelerating
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;
        }

        // If going past top speed, deaccelerate
        else if (curSpeed > topSpeed && !isBraking)
        {
            wheels[0].brakeTorque = deaccelerationSpeed;
            wheels[1].brakeTorque = deaccelerationSpeed;
            wheels[2].brakeTorque = deaccelerationSpeed;
            wheels[3].brakeTorque = deaccelerationSpeed;

            wheels[0].motorTorque = 0;
            wheels[1].motorTorque = 0;
            wheels[2].motorTorque = 0;
            wheels[3].motorTorque = 0;
        }

        //Else if is braking but NOT at min speed, accelerate
        else if(isBraking && curSpeed < minSpeed)
        {
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;

            wheels[0].motorTorque = enginePower;
            wheels[1].motorTorque = enginePower;
            wheels[2].motorTorque = enginePower;
            wheels[3].motorTorque = enginePower;
        }
    }

    void BrakingEffect ()
    {
        // If is in braking trigger
        if (isBraking && curSpeed >= minSpeed)
        {
            // If car needs to be slowed down

                //brakingLights.material = activeBrakeLight;

                //While in "started braking time", lerp to max value and stay there instead of instant
                if (curBrakeTime <= brakeSpeed)
                {
                    curBrakeTime += Time.deltaTime;
                    wheels[0].brakeTorque = Mathf.Lerp(wheels[0].brakeTorque, maxBrakeTorgue, curBrakeTime/ brakeSpeed);
                    wheels[1].brakeTorque = Mathf.Lerp(wheels[1].brakeTorque, maxBrakeTorgue, curBrakeTime / brakeSpeed);
                    wheels[2].brakeTorque = Mathf.Lerp(wheels[2].brakeTorque, maxBrakeTorgue, curBrakeTime / brakeSpeed);
                    wheels[3].brakeTorque = Mathf.Lerp(wheels[3].brakeTorque, maxBrakeTorgue, curBrakeTime / brakeSpeed);
                }
                else
                {
                    wheels[0].brakeTorque = maxBrakeTorgue;
                    wheels[1].brakeTorque = maxBrakeTorgue;
                    wheels[2].brakeTorque = maxBrakeTorgue;
                    wheels[3].brakeTorque = maxBrakeTorgue;

                }

                // Unlerped braking V1
                /*wheels[0].brakeTorque = maxBrakeTorgue;
                wheels[1].brakeTorque = maxBrakeTorgue;
                wheels[2].brakeTorque = maxBrakeTorgue;
                wheels[3].brakeTorque = maxBrakeTorgue;*/

                wheels[0].motorTorque = 0;
                wheels[1].motorTorque = 0;
                wheels[2].motorTorque = 0;
                wheels[3].motorTorque = 0;
        }

        // Else clear braking lights, braking itself should be cleared by movement function
        else
        {
            // Clear braking timer
            curBrakeTime = 0f;

            //brakingLights.material = idleBrakeLight;
        }
    }

    /* == Brainstorming area ==
     * Sensors logic: 
     *               1. Front L/R straight rcs check for objects. Set avoidSensitivity to dodge opposite direction. IF BOTH triggered, use
     *          | | |   one in the middle and check for the rays normal for dodging direction ToDo also braking/reverse logic on these
     *        \ | | | / 2. IF Front L/R straights on same side are NOT triggered, try sensing in angle. Add 1/2 amount to avoidSensitivity
     *         \|_|_|/     to the opposite direction to dodge
     *          |   |
     *       ---|car|--- 3. L/R rcs for sides. IF triggering add 1/4th of avoidSensitivity to dodge
     *          |___|
     *            |  4.
     *  1+. IF BOTH rcs are triggering and the distance between hit point is less than '1/2 rc' -> trigger AI to slow down while dodging.
     *      IF the distance is '1/4 of rc' or less (practically 0) -> brake till stopped -> reverse till hit point is '1/2 of rc' away.
     *      Reverse should check the objects normal to get direction to reverse to. After reverse is done, return to accelerating
     * 
     *  4. Reverse till x distance away from target in front IF nothing is right behind the car. If is, stop reversing and return.
     *    
     * 
     */

    void Sensors()
    {
        // Set up the variables
        detected = false; // Flag for if detected obstacles or not
        float avoidSensitivity = 0f; // Rough value of avoidance needed, being set accordingly of detecting RC. If + try avoid to right, else left
        RaycastHit hit;
        Vector3 pos = transform.position;

        // Setting up angled raycasts angles
        var rightAngle = Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward;
        var leftAngle = Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward;

        // Braking sensor, positioned on front mid. Makes vehicle try to brake if detecting collision.//ToDo something is overriding reverse
        pos += (transform.forward * frontSensorStartPoint) + transform.up;
        if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght * 0.5f, layermask) && goingRightWay)
        {
            detected = true;
            inReverseRcast = true;
            // If car isnt already reversing, try to brake
            if (!reversing)
            {
                if (curSpeed > minSpeed)
                {
                    wheels[0].brakeTorque = deaccelerationSpeed;
                    wheels[1].brakeTorque = deaccelerationSpeed;
                    wheels[2].brakeTorque = deaccelerationSpeed;
                    wheels[3].brakeTorque = deaccelerationSpeed;
                }
            }

            if (debuggingOn)
            {
                Debug.Log("front mid brake sensor hit " + hit.collider.name);
                Debug.DrawLine(pos, hit.point, Color.red);
            }
        }

        //////////////////////////////////////////////////////////////////////
        else if (goingRightWay == false)// Wip wrong way reverse, may get stuck if is at the edge of right- and wrong way at low speed turn
        {
            if (wwReversing) // Reverse for x amount of time if start going wrong way
            {
                detected = true;
                inReverseRcast = true;
                Invoke("WwEndReverse", 3f);

                // Check which way is the next target node, set avoid to reverse in the opposite direction
                if (avoidSensitivity == 0)
                {
                    Vector3 perp = Vector3.Cross(transform.forward, nodes[curNode].position);
                    float direction = Vector3.Dot(perp, transform.up);

                    if (direction < 0)
                        avoidSensitivity -= 1;
                    else
                        avoidSensitivity += 1;
                }

                // If car isnt already reversing, try to brake
                if (!reversing && curSpeed > minSpeed)
                {
                    inReverseRcast = false;

                    wheels[0].brakeTorque = deaccelerationSpeed;
                    wheels[1].brakeTorque = deaccelerationSpeed;
                    wheels[2].brakeTorque = deaccelerationSpeed;
                    wheels[3].brakeTorque = deaccelerationSpeed;
                }

                else
                {
                    // If theres no need to be braking (and no braking by trigger is happening), then reset brakes to 0
                    if (!isBraking)
                    {
                        wheels[0].brakeTorque = 0;
                        wheels[1].brakeTorque = 0;
                        wheels[2].brakeTorque = 0;
                        wheels[3].brakeTorque = 0;
                    }

                    if (!reversing)
                        inReverseRcast = false;
                }
            }

            // After reverse, go forward for another x amount of time OR until youre going right way
            else
            {
                detected = false;
                inReverseRcast = false;
                Invoke("WwStartReverse", 3f);
            }
        }
        /////////////////////////////////////////////////////////////////////

        // Front right straight sensor. If detected obj in front, set flag to true and avoidance to -1f
        pos += (transform.right * frontSensorSideDist);
        if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght * 1.1f, layermask))
        {
            detected = true;
            avoidSensitivity -= 1f;

            if (debuggingOn)
            {
                Debug.Log("front right straight sensor hit " + hit.collider.name);
                Debug.DrawLine(pos, hit.point, Color.yellow);
            }
        }

        // Front Angled right sensor. If detected obj in front, set flag to true and avoidance to -0.5f
        else if (Physics.Raycast(pos, rightAngle, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity -= 0.5f;

            if (debuggingOn)
            {
                Debug.Log("front right angled sensor hit " + hit.collider.name);
                Debug.DrawLine(pos, hit.point, Color.yellow);
            }
        }

        // Front left straight sensor. If detected obj in front, set flag to true and avoidance to 1f
        // Reset the values set above
        pos = transform.position;
        pos += (transform.forward * frontSensorStartPoint) + transform.up;
        pos -= (transform.right * frontSensorSideDist);
        if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght * 1.1f, layermask))
        {
            detected = true;
            avoidSensitivity += 1f;

            if (debuggingOn)
            {
                Debug.Log("front left straight sensor hit " + hit.collider.name);
                Debug.DrawLine(pos, hit.point, Color.yellow);
            }
        }

        // Front angled left sensor. If detected obj in front, set flag to true and to avoidance 0.5
        else if (Physics.Raycast(pos, leftAngle, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 0.5f;

            if (debuggingOn)
            {
                Debug.Log("front left angled sensor hit " + hit.collider.name);
                Debug.DrawLine(pos, hit.point, Color.yellow);
            }
        }

        // Right sideway Sensor. If detected obj in front, set flag to true and avoidance to -0.25f(was -0.5)
        if (Physics.Raycast(transform.position, transform.right, out hit, sidewaySensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity -= 0.25f;

            if (debuggingOn)
            {
                Debug.Log("right side sensor hit " + hit.collider.name);
                Debug.DrawLine(transform.position, hit.point, Color.yellow);
            }
        }

        // Left sideway Sensor. If detected obj in front, set flag to true and avoidance to 0.25f(was 0.5)
        if (Physics.Raycast(transform.position, -transform.right, out hit, sidewaySensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 0.25f;

            if (debuggingOn)
            {
                Debug.Log("left side sensor hit " + hit.collider.name);
                Debug.DrawLine(transform.position, hit.point, Color.yellow);
            }
        }

        // Front mid sensor. If detected obj in front, set flag to true and avoidance to -1 or 1 according the normal of RC hit obj
        // Reset the values set above
        pos = transform.position;
        pos += (transform.forward * frontSensorStartPoint) + transform.up;
        if (avoidSensitivity == 0)
        {
            if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght * 1.25f, layermask))
            {
                // Hit an obstacle, determine the way to evade to by checking the normal between car and the hit.point
                if (hit.normal.x < 0)
                    avoidSensitivity = 1;
                else
                    avoidSensitivity = -1;

                if(debuggingOn)
                    Debug.DrawLine(pos, hit.point, Color.yellow);
            }
        }

        // Start reversing if youve crashed on a wall / slowed down enough and cant dodge object
        Reversing(avoidSensitivity);

        // If some obstacles detected, override the steering with avoidSteer with amount of avoidSensitivity
        if (detected)
        {
            AvoidSteer(avoidSensitivity);
        }
    }

    // Try avoid sensed objects by steering away with the amount of avoidance provided
    void AvoidSteer (float sensitivity)
    {
        wheels[0].steerAngle = avoidSpeed * sensitivity;
        wheels[1].steerAngle = avoidSpeed * sensitivity;
    }

    #region Reversing
    void Reversing (float avoidSensitivity)
    {
        // While not yet reversing
        if(curSpeed < 0.5f && !reversing && inReverseRcast)
        {
            // Reset brakes
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;

            // Start reversing
            Invoke("StartReverse", waitToReverse);
        }

        // While reversing. Reverse until far enough from object
        if(reversing)
        {
            avoidSensitivity *= -1f;

            if (!inReverseRcast)
                reversing = false;
        }
    }

    void StartReverse ()
    {
        reversing = true;
        // Make sure AI reverses for max of reverseForMax time
        Invoke("EndReverse", reverseForMax);
    }

    void EndReverse ()
    {
        if(reversing)
            reversing = false;
    }

    void WwEndReverse ()
    {
        wwReversing = false;
    }

    void WwStartReverse ()
    {
        wwReversing = true;
    }
    #endregion

    //Adds increased grip with speed
    private void ApplyDownForce()
    {
        for(int i = 0; i < wheels.Length; i++)
            wheels[i].attachedRigidbody.AddForce(-transform.up * downforce * wheels[i].attachedRigidbody.velocity.magnitude);
    }

    void CheckDirection()
    {
        // Could make a system where it tracks last node visited -> always compares last node - next node
        // Checking if already going right way, if not then narrow the angle to prevent jamming at 180 angle
        if (!goingRightWay)
            angleOfRightWayDetection = startAngleOfRightwayDetection;
        else
            angleOfRightWayDetection = startAngleOfRightwayDetection + 10;

        // If is facing the next node then is heading to right direction
        if (Vector3.Angle(transform.forward, nodes[curNode].position - transform.position) < angleOfRightWayDetection)
        {
            goingRightWay = true;
        }

        // Else player is going wrong way
        else
        {
            goingRightWay = false;
        }
    }

    private void TractionControl()
    {
        WheelHit wheelhit;

        for (int i = 0; i < 4; i++)
        {
            wheels[i].GetGroundHit(out wheelhit);

            AdjustTorque(wheelhit.forwardSlip);
        }
    }

    private void AdjustTorque(float forwardSlip)
    {
        if (forwardSlip >= slipLimit && curTorque >= 0)
        {
            curTorque -= 10 * tractionControl;
        }
        else
        {
            curTorque += 10 * tractionControl;

            if (curTorque > maxTorque)
            {
                curTorque = maxTorque;
            }
        }
    }

    void OnTriggerEnter (Collider other)
    {
        // Car reached intervalTrigger, send car and trigger info to the laptime system
        if (other.gameObject.tag == "IntervalTrigger")
        {
            intervalManager.AddIntervalTime(gameObject, other.gameObject);
        }
    }

    void OnTriggerStay (Collider other)
    {
        // While is braking area, brake if needed
        if (other.tag == "BrakeTrigger")
        {
            isBraking = true;
        }
    }
    
    void OnTriggerExit (Collider other)
    {// When leaving braking area, stop braking
        if (other.tag == "BrakeTrigger")
        {
            isBraking = false;
        }
    }

    void Setup ()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMass;
        startAngleOfRightwayDetection = angleOfRightWayDetection;

        if (intervalManager == null)
            intervalManager = GameObject.Find("IntervalTriggers").GetComponent<LapTimeSystem>();

        // If wheels are not to be found, find em to prevent errors
        if (wheels.Length != 4 || wheels == null)
        {
            wheels = new WheelCollider[4];
            wheels[0] = gameObject.transform.Find("Wheel_Colliders/FL_Wheel_Car1").GetComponent<WheelCollider>();
            wheels[1] = gameObject.transform.Find("Wheel_Colliders/FR Wheel_Car1").GetComponent<WheelCollider>();
            wheels[2] = gameObject.transform.Find("Wheel_Colliders/BL_Wheel_Car1").GetComponent<WheelCollider>();
            wheels[3] = gameObject.transform.Find("Wheel_Colliders/BR_Wheel_Car1").GetComponent<WheelCollider>();
        }

        // If wheels are not to be found, find em to prevent errors
        if (wheelMeshes.Length != 4 || wheelMeshes == null)
        {
            wheelMeshes = new GameObject[4];
            wheelMeshes[0] = gameObject.transform.Find("Wheel_Meshes/FL_Wheel_Car1").gameObject;
            wheelMeshes[1] = gameObject.transform.Find("Wheel_Meshes/FR Wheel_Car1").gameObject;
            wheelMeshes[2] = gameObject.transform.Find("Wheel_Meshes/BL_Wheel_Car1").gameObject;
            wheelMeshes[3] = gameObject.transform.Find("Wheel_Meshes/BR_Wheel_Car1").gameObject;
        }
    }

    // WIP Information update function for multiplayer purposes
    public void SetCarInformation (float posX, float posY, float velX, float velY, float rotZ)
    {
        transform.position = new Vector3(posX, posY, 0);
        transform.rotation = Quaternion.Euler(0, 0, rotZ);
        // Not doing anything with velocity for now
    }

    // System to unstuck AI after being stuck for x amount of time ToDO: This can be made into invoke function to check each second or so
    void UnstuckSystem ()
    {
        if(curSpeed <= 1f)
        {
            unstuckTimer += Time.deltaTime;

            if(unstuckTime <= unstuckTimer)
            {
                // Reset the car location to last visited node !! Does not yet reset rotation !!
                unstuckTimer = 0;
                int lastNode = curNode-1;

                if (lastNode < 0)
                    lastNode = nodes.Count;

                transform.position = nodes[lastNode].position;
            }
        }
    }

    void AlignWheels()
    {
        // Get rotation of the wheel colliders and apply to wheel meshes
        for (int i = 0; i < wheelMeshes.Length; i++)
        {
            Quaternion tireRot;
            Vector3 tirePos;
            wheels[i].GetWorldPose(out tirePos, out tireRot);

            wheelMeshes[i].transform.rotation = tireRot;
        }
    }
}
