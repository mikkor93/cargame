﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiCarEngineV3 : MonoBehaviour {

    [Header("Debugging Tools")]
    public bool debuggingOn;
    [Header("Ai Car Vars")]
    [Range(0, 2500)] public float enginePower = 1000f;
    [Range(0, 2500)] public float maxTorque = 1000f;
    private float curTorque;
    public float curSpeed;
    [Range(0, 250)] public float topSpeed = 150f;
    [Range(0, 250)] public float minSpeed = 10f;
    [Range(0, 180)] public float maxSteerAngle = 45f;
    [Range(0, 2)] public float steerSpeed = 0.25f;
    [Range(0,3)] public int steeringVersion = 1;
    [Range(0, 100)] public float distFromNode = 3f;
    [Range(10, 100)] public float slipLimit = 50;
    [Range(50, 500)] public float downforce = 340;
    
    [Range(0, 1)] public float tractionControl;
    [Range(0, 500)] public float maxBrakeTorgue = 20f;
    public bool isBraking;
    public bool reversing;
    public bool goingRightWay;
    public WheelCollider[] wheels;
    public GameObject[] wheelMeshes;

    [Header("Sensors")]
    public LayerMask layermask;
    [Range(0, 50)] public float sensorLenght = 5f;
    [Range(0, 50)] public float frontSensorStartPoint = 5f;
    [Range(0, 50)] public float frontSensorSideDist = 5f;
    [Range(0, 90)] public float frontSensorAngle = 30f;
    [Range(0, 50)] public float sidewaySensorLenght = 5;
    [Range(0, 100)] public float avoidSpeed = 10f;
    private float avoidSensitivity = 0f;
    private bool detected;
    private bool farEnough = true;

    [Range(0, 180)] public float angleOfRightWayDetection = 90f;
    private float startAngleOfRightwayDetection;

    [Header("Helpers")]
    public LapTimeSystem intervalManager;
    public Transform path;
    public Vector3 centerOfMass;
    private List<Transform> nodes;
    private int curNode = 0;
    private bool didChangeStatesLastFrame;

    private Rigidbody rb;

    void Start ()
    {
        Setup ();

        // Check if car is going the right way every 1 seconds
        InvokeRepeating("CheckDirection", 0f, 1f);
        // Setup the rotation of the wheel meshes every 0.x seconds
        InvokeRepeating("AlignWheels", 0f, 0.1f);
    }
	

	void FixedUpdate ()//ToDo need to add the logic for "if going wrong way"
    {
        // Sensors logic and debugging version of it
        Sensors();
        if (debuggingOn)
            DebuggingSensors();

        // Forwards/backwards movement
        Movement();
        TractionControl();
        ApplyDownForce();

        // Steering
        if (!detected)
            ApplySteering();
        else
            AvoidSteer(avoidSensitivity);
    }
    #region Steering
    void ApplySteering()
    {
        // Get the relative vector between car and the waypoint (instead of 0,0)
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[curNode].position);
        //relativeVector /= relativeVector.magnitude;

        // Calculate and apply steering to the wheel colliders
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;

        //Unlerped steering V1
        if (steeringVersion == 1)
        {
            wheels[0].steerAngle = newSteer;
            wheels[1].steerAngle = newSteer;
        }

        // Set the next node to go to when reached current one
        if (relativeVector.magnitude <= distFromNode)
        {
            if (curNode < nodes.Count - 1)
                curNode++;
            else
                curNode = 0;
        }
    }

    // Try avoid sensed objects by steering away with the amount of avoidance provided
    void AvoidSteer(float sensitivity)
    {
        wheels[0].steerAngle = avoidSpeed * sensitivity;
        wheels[1].steerAngle = avoidSpeed * sensitivity;
    }
    #endregion
    #region Movement
    void Movement ()
    {
        // If car is not in brake zone or is not reversing, go forward
        if(!isBraking && !reversing)
        {
            // Make sure theres no brakes left affecting
            ClearBrakes();
            // If car has not capped the top speed, keep accelerating
            if (curSpeed <= topSpeed)
            {
                Accelerate();
            }
            // If car is overcapping the top speed, "deaccelerate" till just below cap
            else
            {
                wheels[0].motorTorque = -enginePower;
                wheels[1].motorTorque = -enginePower;
                wheels[2].motorTorque = -enginePower;
                wheels[3].motorTorque = -enginePower;
            }
        }

        // If car is in brake zone (or trigger brakes) and is not reversing
        else if (isBraking && !reversing)
        {
            // If car is in braking but is below min speed, clear brakes and accelerate
            if (curSpeed < minSpeed)
            {
                ClearBrakes();

                Accelerate();
            }

            // Else brake toDo lerped brakes
            else
            {
                wheels[0].brakeTorque = maxBrakeTorgue;
                wheels[1].brakeTorque = maxBrakeTorgue;
                wheels[2].brakeTorque = maxBrakeTorgue;
                wheels[3].brakeTorque = maxBrakeTorgue;
            }
        }

        else if (reversing)
        {
            // Make sure there is no brakes affecting
            ClearBrakes();

            // If is reversing and gets far enough from object, clear reverse and start going forward(farEnough is set by sensors)
            if (farEnough)
            {
                reversing = false;
                Accelerate();
            }

            // While car still needs to be reversing
            else
            {
                wheels[0].motorTorque = -enginePower;
                wheels[1].motorTorque = -enginePower;
                wheels[2].motorTorque = -enginePower;
                wheels[3].motorTorque = -enginePower;
            }
        }
        else
        {
            Debug.Log("Getting null movement?!");
        }
    }
    #endregion

 /* == Brainstorming area ==
 * Sensors logic: 
 *               1. Front L/R straight rcs check for objects. Set avoidSensitivity to dodge opposite direction. IF BOTH triggered, use
 *          | | |   one in the middle and check for the rays normal for dodging direction ToDo also braking/reverse logic on these
 *        \ | | | / 2. IF Front L/R straights on same side are NOT triggered, try sensing in angle. Add 1/2 amount to avoidSensitivity
 *         \|_|_|/     to the opposite direction to dodge
 *          |   |
 *       ---|car|--- 3. L/R rcs for sides. IF triggering add 1/4th of avoidSensitivity to dodge
 *          |___|
 *            |  4.
 *  1+. IF BOTH rcs are triggering and the distance between hit point is less than '1/2 rc' -> trigger AI to slow down while dodging.
 *      IF the distance is '1/4 of rc' or less (practically 0) -> brake till stopped -> reverse till hit point is '1/2 of rc' away.
 *      Reverse should check the objects normal to get direction to reverse to. After reverse is done, return to accelerating
 * 
 *  4. Reverse till x distance away from target in front IF nothing is right behind the car. If is, stop reversing and return.
 *    
 * 
 */
    void Sensors ()
    {
        // Clear detection states
        detected = false;
        bool rfDetected = false;
        bool lfDetected = false;
        // Rough value of avoidance needed, being set accordingly of detecting RC. If + try avoid to right, else left
        avoidSensitivity = 0f;
        RaycastHit hit;
        // Get cars pos with fixed sensor height
        Vector3 pos = transform.position + transform.up * 0.5f;

        // Setup angled RCs angles
        var rightAngle = Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward;
        var leftAngle = Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward;

        //>> ======================= FRONT SENSORS ===============================
        // Right sensors start pos (both straight and angled)
        Vector3 rpos = pos + (transform.right * frontSensorSideDist) + (transform.forward * frontSensorStartPoint);

        // Front right sensor. If detected obj in fron, set flag to true and avoidance to -1f
        if (Physics.Raycast(rpos, transform.forward, out hit, sensorLenght * 1.1f, layermask))
        {
            detected = true;
            rfDetected = true;
            avoidSensitivity -= 1f;
        }

        // Angled front right sensor. If detected obj in front, set flag to true and avoidance to -0.5f
        
        else if (Physics.Raycast(rpos, rightAngle, out hit, sensorLenght * 0.9f, layermask))
        {
            detected = true;
            avoidSensitivity -= 0.5f;
        }

        // Left sensors start pos (both straight and angled)
        Vector3 lPos = pos - (transform.right * frontSensorSideDist) + (transform.forward * frontSensorStartPoint);

        // Front left sensor. If detected obj in front, set flag to true and avoidance to 1f
        if(Physics.Raycast(lPos, transform.forward, out hit, sensorLenght * 1.1f, layermask))
        {
            detected = true;
            lfDetected = true;
            avoidSensitivity += 1f;
        }

        // Angled front left sensor. If detected obj in front, set flag to true and to avoidance 0.5
        else if (Physics.Raycast(lPos, leftAngle, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 0.5f;
        }

        // If both straight sensor detected, then shoot one ray in middle. If it hits aswell. Get that rays normal and avoid according it
        bool didChangeStates = false;
        if (rfDetected && lfDetected)
        {
            if (Physics.Raycast(pos + (transform.forward * frontSensorStartPoint), transform.forward, out hit, sensorLenght * 1.2f, layermask))
            {
                didChangeStates = true;
                didChangeStatesLastFrame = true;
                // If the hit distance to the object is over 1/2 RC lenght, just try to dodge it
                if (hit.distance >= ((sensorLenght * 1.2f) * 0.5f))
                {
                    if (hit.normal.x < 0)
                        avoidSensitivity = 1;
                    else
                        avoidSensitivity = -1;
                }

                // If distance is between the 1/2 and 1/4 of sensor lenght, then try to slow down while dodging
                else if (hit.distance < ((sensorLenght * 1.2f) * 0.5f) && hit.distance > ((sensorLenght * 1.2f) * 0.25f))
                {
                    if (hit.normal.x < 0)
                        avoidSensitivity = 1;
                    else
                        avoidSensitivity = -1;

                    isBraking = true; // ToDo this should be cleared somewhere
                }

                // else if distance to object is less than 1/4th, start reversing with mirrored avoidSensitivity
                else if (hit.distance < ((sensorLenght * 1.2f) * 0.25f)) // ToDo check if 0.25f is proper amount for reverse check
                {
                    if (hit.normal.x < 0)
                        avoidSensitivity = -1;
                    else
                        avoidSensitivity = 1;

                    reversing = true;
                }
            }
        }

        // Practically bool check if car has left the ray check since last frame. If is, reset states set above
        if(didChangeStates != didChangeStatesLastFrame)
        {
            // Reset the state check bool 
            didChangeStatesLastFrame = false;
            isBraking = false;
            reversing = false; // ToDo This prolly should have its own RC function to clear? Atleast add ray on back while reversing
        }
        //<< ======================= FRONT SENSORS ===============================

        //>> ======================= SIDE SENSORS ===============================
        // Right sideway Sensor. If detected obj, set flag to true and avoidance to -0.25f
        if (Physics.Raycast(pos, transform.right, out hit, sidewaySensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity -= 0.25f;
        }

        // Left sideway Sensor. If detected obj, set flag to true and avoidance to 0.25f
        if (Physics.Raycast(pos, -transform.right, out hit, sidewaySensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 0.25f;
        }
        //<< ======================= SIDE SENSORS ===============================

        // Back sensor for reverse clearing purposes. If car reverses into a wall, clear the reverse
        if(reversing)
        {
            if(Physics.Raycast(pos, - transform.forward, out hit, sensorLenght * 0.25f, layermask)) // ToDo check if 0.25f is good amount
            {
                reversing = false;
            }
        }
    }

    #region Helpers
    void AlignWheels()
    {
        // Get rotation of the wheel colliders and apply to wheel meshes
        for (int i = 0; i < wheelMeshes.Length; i++)
        {
            Quaternion tireRot;
            Vector3 tirePos;
            wheels[i].GetWorldPose(out tirePos, out tireRot);

            wheelMeshes[i].transform.rotation = tireRot;
        }
    }

    void ApplyDownForce()
    {
        for (int i = 0; i < wheels.Length; i++)
            wheels[i].attachedRigidbody.AddForce(-transform.up * downforce * wheels[i].attachedRigidbody.velocity.magnitude);
    }

    void TractionControl()
    {
        WheelHit wheelhit;

        for (int i = 0; i < wheels.Length; i++)
        {
            wheels[i].GetGroundHit(out wheelhit);

            AdjustTorque(wheelhit.forwardSlip);
        }
    }

    void AdjustTorque(float forwardSlip)
    {
        if (forwardSlip >= slipLimit && curTorque >= 0)
            curTorque -= 10 * tractionControl;
        else
            curTorque += 10 * tractionControl;
        {
            if (curTorque > maxTorque)
                curTorque = maxTorque;
        }
    }

    void CheckDirection()
    {
        // Could make a system where it tracks last node visited -> always compares last node - next node
        // Checking if already going right way, if not then narrow the angle to prevent jamming at 180 angle
        if (!goingRightWay)
            angleOfRightWayDetection = startAngleOfRightwayDetection;
        else
            angleOfRightWayDetection = startAngleOfRightwayDetection + 10;

        // If is facing the next node then is heading to right direction
        if (Vector3.Angle(transform.forward, nodes[curNode].position - transform.position) < angleOfRightWayDetection)
        {
            goingRightWay = true;
        }

        // Else player is going wrong way
        else
        {
            goingRightWay = false;
        }
    }

    void ClearBrakes()
    {
        wheels[0].brakeTorque = 0;
        wheels[1].brakeTorque = 0;
        wheels[2].brakeTorque = 0;
        wheels[3].brakeTorque = 0;
    }

    void Accelerate()
    {
        wheels[0].motorTorque = enginePower;
        wheels[1].motorTorque = enginePower;
        wheels[2].motorTorque = enginePower;
        wheels[3].motorTorque = enginePower;
    }
#endregion
    #region Triggers
    void OnTriggerEnter(Collider other)
    {
        // Car reached intervalTrigger, send car and trigger info to the laptime system
        if (other.gameObject.tag == "IntervalTrigger")
            intervalManager.AddIntervalTime(gameObject, other.gameObject);
    }

    void OnTriggerStay(Collider other)
    {
        // While is braking area, brake if needed
        if (other.tag == "BrakeTrigger")
            isBraking = true;
    }

    void OnTriggerExit(Collider other)
    {
        // When leaving braking area, stop braking
        if (other.tag == "BrakeTrigger")
            isBraking = false;
    }
    #endregion
    #region Setup
    void Setup()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMass;
        startAngleOfRightwayDetection = angleOfRightWayDetection;

        if (intervalManager == null)
            intervalManager = GameObject.Find("IntervalTriggers").GetComponent<LapTimeSystem>();

        // If wheels are not to be found, find em to prevent errors
        if (wheels.Length != 4 || wheels == null)
        {
            wheels = new WheelCollider[4];
            wheels[0] = gameObject.transform.Find("Wheel_Colliders/FL_Wheel_Car1").GetComponent<WheelCollider>();
            wheels[1] = gameObject.transform.Find("Wheel_Colliders/FR Wheel_Car1").GetComponent<WheelCollider>();
            wheels[2] = gameObject.transform.Find("Wheel_Colliders/BL_Wheel_Car1").GetComponent<WheelCollider>();
            wheels[3] = gameObject.transform.Find("Wheel_Colliders/BR_Wheel_Car1").GetComponent<WheelCollider>();
        }

        // If wheels are not to be found, find em to prevent errors
        if (wheelMeshes.Length != 4 || wheelMeshes == null)
        {
            wheelMeshes = new GameObject[4];
            wheelMeshes[0] = gameObject.transform.Find("Wheel_Meshes/FL_Wheel_Car1").gameObject;
            wheelMeshes[1] = gameObject.transform.Find("Wheel_Meshes/FR Wheel_Car1").gameObject;
            wheelMeshes[2] = gameObject.transform.Find("Wheel_Meshes/BL_Wheel_Car1").gameObject;
            wheelMeshes[3] = gameObject.transform.Find("Wheel_Meshes/BR_Wheel_Car1").gameObject;
        }

        GetPath();
    }

    void GetPath()
    {
        if (path == null)
            path = GameObject.Find("PathV2").GetComponent<Transform>();
        // Get the path transforms
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
    }
    #endregion

    void DebuggingSensors ()
    {

        bool rfDetected = false;
        bool lfDetected = false;
        bool raDetected = false;
        bool laDetected = false;
        RaycastHit hit;
        // Get cars pos with fixed sensor height
        Vector3 pos = transform.position + transform.up * 0.5f;

        // Setup angled RCs angles
        var rightAngle = Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward;
        var leftAngle = Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward;

        //>> ======================= FRONT SENSORS ===============================
        // Right sensors start pos (both straight and angled)
        Vector3 rpos = pos + (transform.right * frontSensorSideDist) + (transform.forward * frontSensorStartPoint);

        // Front right sensor. If detected obj in fron, set flag to true and avoidance to -1f
        if (Physics.Raycast(rpos, transform.forward, out hit, sensorLenght * 1.1f, layermask))
        {
            rfDetected = true;
            Debug.DrawRay(rpos, transform.forward * (sensorLenght * 1.1f), Color.red);
        }

        if(!rfDetected)
        {
            Debug.DrawRay(rpos, transform.forward * (sensorLenght * 1.1f), Color.green);
        }

        // Angled front right sensor. If detected obj in front, set flag to true and avoidance to -0.5f
        else if (Physics.Raycast(rpos, rightAngle, out hit, sensorLenght * 0.9f, layermask))
        {
            raDetected = true;
            Debug.DrawRay(rpos, rightAngle * (sensorLenght * 0.9f), Color.red);
        }

        if (!raDetected)
        {
            Debug.DrawRay(rpos, rightAngle * (sensorLenght * 0.9f), Color.green);
        }

        // Left sensors start pos (both straight and angled)
        Vector3 lPos = pos - (transform.right * frontSensorSideDist) + (transform.forward * frontSensorStartPoint);

        // Front left sensor. If detected obj in front, set flag to true and avoidance to 1f
        if (Physics.Raycast(lPos, transform.forward, out hit, sensorLenght * 1.1f, layermask))
        {
            lfDetected = true;
            Debug.DrawRay(lPos, transform.forward * (sensorLenght * 1.1f), Color.red);
        }

        if(!lfDetected)
        {
            Debug.DrawRay(lPos, transform.forward * (sensorLenght * 1.1f), Color.green);
        }

        // Angled front left sensor. If detected obj in front, set flag to true and to avoidance 0.5
        else if (Physics.Raycast(lPos, leftAngle, out hit, sensorLenght, layermask))
        {
            laDetected = true;
            Debug.DrawRay(lPos, leftAngle * (sensorLenght * 0.9f), Color.red);
        }

        if (!laDetected)
        {
            Debug.DrawRay(lPos, leftAngle * (sensorLenght * 0.9f), Color.green);
        }

        // If both straight sensor detected, then shoot one ray in middle. If it hits aswell. Get that rays normal and avoid according it
        bool didChangeStates = false;
        if (rfDetected && lfDetected)
        {
            if (Physics.Raycast(pos + (transform.forward * frontSensorStartPoint), transform.forward, out hit, sensorLenght * 1.2f, layermask))
            {
                Debug.DrawRay(pos + (transform.forward * frontSensorStartPoint), transform.forward * (sensorLenght * 1.2f), Color.red);
                didChangeStates = true;
                didChangeStatesLastFrame = true;
                // If the hit distance to the object is over 1/2 RC lenght, just try to dodge it
                if (hit.distance >= ((sensorLenght * 1.2f) * 0.5f))
                {

                }

                // If distance is between the 1/2 and 1/4 of sensor lenght, then try to slow down while dodging
                else if (hit.distance < ((sensorLenght * 1.2f) * 0.5f) && hit.distance > ((sensorLenght * 1.2f) * 0.25f))
                {
                    Debug.Log("isBraking = true");
                }

                // else if distance to object is less than 1/4th, start reversing with mirrored avoidSensitivity
                else if (hit.distance < ((sensorLenght * 1.2f) * 0.25f)) // ToDo check if 0.25f is proper amount for reverse check
                {
                    Debug.Log("reversing = true");
                }
            }
            else
                Debug.DrawRay(pos + (transform.forward * frontSensorStartPoint), transform.forward * (sensorLenght * 1.2f), Color.green);
        }

        // Practically bool check if car has left the ray check since last frame. If is, reset states set above
        if (didChangeStates != didChangeStatesLastFrame)
        {
            // Reset the state check bool 
            didChangeStatesLastFrame = false;
            Debug.Log("isBraking and reversing = false");
            reversing = false;
        }
        //<< ======================= FRONT SENSORS ===============================
        bool rss = false;
        bool lss = false;
        //>> ======================= SIDE SENSORS ===============================
        // Right sideway Sensor. If detected obj, set flag to true and avoidance to -0.25f
        if (Physics.Raycast(pos + (transform.right * frontSensorSideDist), transform.right, out hit, sidewaySensorLenght, layermask))
        {
            rss = true;
            Debug.DrawRay(pos + (transform.right * frontSensorSideDist), transform.right * sidewaySensorLenght, Color.red);
        }

        if(!rss)
        {
            Debug.DrawRay(pos + (transform.right * frontSensorSideDist), transform.right * sidewaySensorLenght, Color.green);
        }

        // Left sideway Sensor. If detected obj, set flag to true and avoidance to 0.25f
        if (Physics.Raycast(pos - (transform.right * frontSensorSideDist), -transform.right, out hit, sidewaySensorLenght, layermask))
        {
            Debug.DrawRay(pos - (transform.right * frontSensorSideDist), -transform.right * sidewaySensorLenght, Color.red);
            lss = true;
        }

        if(!lss)
        {
            Debug.DrawRay(pos - (transform.right * frontSensorSideDist), -transform.right * sidewaySensorLenght, Color.green);
        }
        //<< ======================= SIDE SENSORS ===============================
        bool rvs = false;
        // Back sensor for reverse clearing purposes. If car reverses into a wall, clear the reverse
        if (reversing)
        {
            if (Physics.Raycast(pos, -transform.forward, out hit, sensorLenght * 0.25f, layermask)) // ToDo check if 0.25f is good amount
            {
                rvs = true;
                reversing = false;
                Debug.DrawRay(pos, -transform.forward * sensorLenght * 0.25f, Color.red);
            }
        }
        if(!rvs)
        {
            Debug.DrawRay(pos, -transform.forward * sensorLenght * 0.25f, Color.green);
        }
    }
}
