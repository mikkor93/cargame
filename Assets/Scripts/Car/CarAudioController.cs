﻿using UnityEngine;
using System.Collections;

public class CarAudioController : MonoBehaviour {

    public AudioClip A_Acceleration;
    public AudioClip A_DeAcceleration;

    public float topSpeed;
    public float pitchMultiplier = 1f;                                          // Used for altering the pitch of audio clips
    public float lowPitchMin = 1f;                                              // The lowest possible pitch for the low sounds
    public float lowPitchMax = 6f;                                              // The highest possible pitch for the low sounds
    public float highPitchMultiplier = 0.25f;

    private CarControllerV2 carController;
    private AudioSource audio;
    private Rigidbody rb;
    private PlayerInput playerInput;


    private void Start()
    {
        topSpeed = GetComponent<CarControllerV2>().maxSpeed;

        carController = GetComponent<CarControllerV2>();
        playerInput = GetComponent<PlayerInput>();
        rb = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        // The pitch is interpolated between the min and max values, according to the car's revs.
        float pitch = ULerp(lowPitchMin, lowPitchMax, carController.Revs);

        // clamp to minimum pitch (note, not clamped to max for high revs while burning out)
        pitch = Mathf.Min(lowPitchMax, pitch);

        // for 1 channel engine sound
        audio.pitch = pitch * pitchMultiplier * highPitchMultiplier;
    }

    /*public void SelectClip() {
        if(playerInput.throttle) {
            audio.clip = A_Acceleration;
        }

        if(playerInput.brake) {
            audio.clip = A_DeAcceleration;
        }
    }*/

    // unclamped versions of Lerp and Inverse Lerp, to allow value to exceed the from-to range
    private static float ULerp(float from, float to, float value)
    {
        return (1.0f - value) * from + value * to;
    }
}
