﻿using UnityEngine;
using System.Collections;

public class AiWheelControl : MonoBehaviour {

    public WheelCollider myWheelCollider;
    public float fixValue;

    //ToDo Set bool for front n back tires to save some performance of backtires trying to see if they need to rotate

	void Awake ()
    {
        //fixValue = transform.rotation.y;
	}
	

	void FixedUpdate ()//Temp disabled as the tires start to lag behind when moving aka ToDo check this script
    {
        transform.Rotate(0, 0, -myWheelCollider.rpm/60*360*Time.deltaTime);

        // Calculate and set the rotation of the tires
        var localEulers = transform.localEulerAngles;
        localEulers.y = myWheelCollider.steerAngle - transform.localEulerAngles.x + fixValue;
        transform.localEulerAngles = localEulers;

        // Get the suspension for the tires
        RaycastHit hit;
        Vector3 wheelPos;

        if(Physics.Raycast(myWheelCollider.transform.position, -myWheelCollider.transform.up, out hit, myWheelCollider.radius + myWheelCollider.suspensionDistance))
            wheelPos = hit.point + myWheelCollider.transform.up * myWheelCollider.radius;

        else
            wheelPos = myWheelCollider.transform.position - myWheelCollider.transform.up * -myWheelCollider.suspensionDistance;

        transform.position = wheelPos;
    }
}
