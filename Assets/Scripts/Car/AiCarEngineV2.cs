﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AiCarEngineV2 : MonoBehaviour
{

    [Header("Ai Car Vars")]
    public Transform path;
    public Vector3 centerOfMass;
    public float maxSteerAngle = 45f;
    public float enginePower = 20f;
    public float curSpeed = 20f;
    public float curSpeed2;
    public float topSpeed = 150f;
    public float minSpeed = 10f;
    public float deaccelerationSpeed = 10f;
    public float maxBrakeTorgue = 20f;
    public float distFromNode = 3f;
    //public WheelCollider wheelFL;
    //public WheelCollider wheelFR;
    public WheelCollider[] wheels;

    private List<Transform> nodes;
    private int curNode = 0;

    [Header("Braking Light")]
    public Renderer brakingLights;
    public Material idleBrakeLight;
    public Material activeBrakeLight;
    public bool isBraking;

    [Header("Sensors")]
    public LayerMask layermask;
    public float sensorLenght = 5f;
    public float frontSensorStartPoint = 5f;
    public float frontSensorSideDist = 5f;
    public float frontSensorAngle = 30f;
    public float sidewaySensorLenght = 5;
    public float avoidSpeed = 10f;
    private bool detected;

    public bool goingRightWay;
    public float angleOfRightWayDetection = 90f;

    public bool reversing;
    // V1   public float reverseCounter;
    public float waitToReverse = 1f;
    // V1   public float reverseFor = 1.5f;
    public bool inReverseRcast;

    public LapTimeSystem intervalManager;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMass;
        GetPath();

        InvokeRepeating("CheckDirection", 0f, 1f);
    }
    #region Get Path
    void GetPath()
    {
        // Get the path transforms
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
    }
    #endregion
    void FixedUpdate()
    {
        // If theres no obstacles detected by the sensors, apply normal steering
        if (!detected)
            ApplySteer();

        Movement();
        BrakingEffect();
        Sensors();
    }

    void ApplySteer()
    {
        // Get the relative vector between car and the waypoint (instead of 0,0)
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[curNode].position);
        //relativeVector /= relativeVector.magnitude;

        // Calculate and apply steering to the wheel colliders
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
        //wheelFL.steerAngle = newSteer;
        //wheelFR.steerAngle = newSteer;
        wheels[0].steerAngle = newSteer;
        wheels[1].steerAngle = newSteer;

        // Set the next node to go to
        if (relativeVector.magnitude <= distFromNode)
        {
            if (curNode < nodes.Count - 1)
                curNode++;
            else
                curNode = 0;
        }
    }

    // Apply throttle
    void Movement()
    {
        // Current speed being calculated from RPM and rounded
        /*     curSpeed = 2 * Mathf.PI * wheelFL.radius*((wheelFL.rpm + wheelFR.rpm)*0.5f)*60/1000; // Temp solution to take curSpeed from rbs velocity currently active, remove these comms and curspeed = curspeed2 to go back to original
             curSpeed = Mathf.Round(curSpeed);*/
        // Cur speed calculated by the rigidbody
        curSpeed2 = rb.velocity.magnitude;
        curSpeed = curSpeed2;

        // While hasnt topped maxSpeed and is not braking, accelerate with enginePower
        if (curSpeed <= topSpeed && !isBraking)
        {
            // If not reversing, accelerate
            if (!reversing)
            {
                //wheelFL.motorTorque = enginePower;
                //wheelFR.motorTorque = enginePower;
                wheels[0].motorTorque = enginePower;
                wheels[1].motorTorque = enginePower;
                wheels[2].motorTorque = enginePower;
                wheels[3].motorTorque = enginePower;
            }
            // Else if reversing, apply reverse as negative enginepower
            else
            {
                //wheelFL.motorTorque = -enginePower;
                //wheelFR.motorTorque = -enginePower;
                wheels[0].motorTorque = -enginePower;
                wheels[1].motorTorque = -enginePower;
                wheels[2].motorTorque = -enginePower;
                wheels[3].motorTorque = -enginePower;
            }

            // Make sure theres not brakeTorque applied when accelerating
            //wheelFL.brakeTorque = 0;
            //wheelFR.brakeTorque = 0;
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;
        }

        // If braking, deaccelerate
        else if (!isBraking)
        {
            //wheelFL.brakeTorque = deaccelerationSpeed;
            //wheelFR.brakeTorque = deaccelerationSpeed;
            wheels[0].brakeTorque = deaccelerationSpeed;
            wheels[1].brakeTorque = deaccelerationSpeed;
            wheels[2].brakeTorque = deaccelerationSpeed;
            wheels[3].brakeTorque = deaccelerationSpeed;

            //wheelFL.motorTorque = 0;
            //wheelFR.motorTorque = 0;
            wheels[0].motorTorque = 0;
            wheels[1].motorTorque = 0;
            wheels[2].motorTorque = 0;
            wheels[3].motorTorque = 0;
        }
        else
        {
            //wheelFL.brakeTorque = 0;
            //wheelFR.brakeTorque = 0;
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;

            //wheelFL.motorTorque = enginePower;
            //wheelFR.motorTorque = enginePower;
            wheels[0].motorTorque = enginePower;
            wheels[1].motorTorque = enginePower;
            wheels[2].motorTorque = enginePower;
            wheels[3].motorTorque = enginePower;
        }
    }

    void BrakingEffect()
    {
        // If is in braking trigger
        if (isBraking)
        {
            // If car needs to be slowed down
            if (curSpeed >= minSpeed)
            {
                //brakingLights.material = activeBrakeLight;

                //wheelFL.brakeTorque = maxBrakeTorgue;
                //wheelFR.brakeTorque = maxBrakeTorgue;
                wheels[0].brakeTorque = maxBrakeTorgue;
                wheels[1].brakeTorque = maxBrakeTorgue;
                wheels[2].brakeTorque = maxBrakeTorgue;
                wheels[3].brakeTorque = maxBrakeTorgue;

                //wheelFL.motorTorque = 0;
                //wheelFR.motorTorque = 0;
                wheels[0].motorTorque = 0;
                wheels[1].motorTorque = 0;
                wheels[2].motorTorque = 0;
                wheels[3].motorTorque = 0;
            }
            // If does not need to be slowed down, accelerate
            else
            {
                //wheelFL.brakeTorque = 0;
                //wheelFR.brakeTorque = 0;
                wheels[0].brakeTorque = 0;
                wheels[1].brakeTorque = 0;
                wheels[2].brakeTorque = 0;
                wheels[3].brakeTorque = 0;

                //wheelFL.motorTorque = enginePower;
                //wheelFR.motorTorque = enginePower;
                wheels[0].motorTorque = enginePower;
                wheels[1].motorTorque = enginePower;
                wheels[2].motorTorque = enginePower;
                wheels[3].motorTorque = enginePower;
            }
        }

        // Else clear braking lights, braking itself should be cleared by " void movement"
        else
        {
            //brakingLights.material = idleBrakeLight;
            //Debug.Log("Not Braking");
        }
    }

    void Sensors()
    {
        // Set up the variables
        detected = false; // Flag for if detected obstacles or not
        float avoidSensitivity = 0f; // Rough value of avoidance needed, being set accordingly of detecting RC. If + try avoid to right, else left
        RaycastHit hit;
        Vector3 pos = transform.position;

        // Setting up angled raycasts angles
        var rightAngle = Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward;
        var leftAngle = Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward;

        // Braking sensor, positioned on front mid. Makes vehicle try to brake if detecting collision.
        pos += (transform.forward * frontSensorStartPoint) + transform.up;
        if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght, layermask))
        {
            detected = true;
            inReverseRcast = true;
            // If car isnt already reversing, try to brake
            if (!reversing)
            {
                //wheelFL.brakeTorque = deaccelerationSpeed;
                //wheelFR.brakeTorque = deaccelerationSpeed;
                wheels[0].brakeTorque = deaccelerationSpeed;
                wheels[1].brakeTorque = deaccelerationSpeed;
                wheels[2].brakeTorque = deaccelerationSpeed;
                wheels[3].brakeTorque = deaccelerationSpeed;
            }
            Debug.DrawLine(pos, hit.point, Color.red);
        }

        //////////////////////////////////////////////////////////////////////
        if (goingRightWay == false)// Easy attempt of making wrong way reverse
        {//Ongelma; juuttuu 180 asteen kulmaan rightway/wrongwayhin koska funktio muuttuu samantien
            // Ehkä kulma vois olla esim 75 kun AI palaa "wrong waystä"? tällöin se oli väkisinkin oikeesee suuntaan menossa
            detected = true;
            inReverseRcast = true;

            if (avoidSensitivity == 0)//kääntyy ees jonnekki suuntaan pakittamaan alkaessa, ei vaan pakita suoraan jos ei ole detectioneita
                avoidSensitivity = -1;
            // If car isnt already reversing, try to brake
            if (!reversing)
            {
                //wheelFL.brakeTorque = deaccelerationSpeed;
                //wheelFR.brakeTorque = deaccelerationSpeed;
                wheels[0].brakeTorque = deaccelerationSpeed;
                wheels[1].brakeTorque = deaccelerationSpeed;
                wheels[2].brakeTorque = deaccelerationSpeed;
                wheels[3].brakeTorque = deaccelerationSpeed;
            }
        }
        /////////////////////////////////////////////////////////////////////

        else
        {
            // If theres no need to be braking (and no braking by trigger is happening), then reset brakes to 0
            if (!isBraking)
            {
                //wheelFL.brakeTorque = 0;
                //wheelFR.brakeTorque = 0;
                wheels[0].brakeTorque = 0;
                wheels[1].brakeTorque = 0;
                wheels[2].brakeTorque = 0;
                wheels[3].brakeTorque = 0;
            }
            inReverseRcast = false;
        }

        // Front right straight sensor. If detected obj in front, set flag to true and avoidance to -1f
        pos += (transform.right * frontSensorSideDist);
        if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity -= 1f;
            Debug.DrawLine(pos, hit.point, Color.yellow);
        }

        // Front Angled right sensor. If detected obj in front, set flag to true and avoidance to -0.5f
        else if (Physics.Raycast(pos, rightAngle, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity -= 0.5f;
            Debug.DrawLine(pos, hit.point, Color.yellow);
        }

        // Front left straight sensor. If detected obj in front, set flag to true and avoidance to 1f
        // Reset the values set above
        pos = transform.position;
        pos += (transform.forward * frontSensorStartPoint) + transform.up;
        pos -= (transform.right * frontSensorSideDist);
        if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 1f;
            Debug.DrawLine(pos, hit.point, Color.yellow);
        }

        // Front angled left sensor. If detected obj in front, set flag to true and avoidance to 0.5f
        else if (Physics.Raycast(pos, leftAngle, out hit, sensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 0.5f;
            Debug.DrawLine(pos, hit.point, Color.yellow);
        }

        // Right sideway Sensor. If detected obj in front, set flag to true and avoidance to -0.5f
        if (Physics.Raycast(transform.position, transform.right, out hit, sidewaySensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity -= 0.5f;
            Debug.DrawLine(transform.position, hit.point, Color.yellow);
        }

        // Left sideway Sensor. If detected obj in front, set flag to true and avoidance to 0.5f
        if (Physics.Raycast(transform.position, -transform.right, out hit, sidewaySensorLenght, layermask))
        {
            detected = true;
            avoidSensitivity += 0.5f;
            Debug.DrawLine(transform.position, hit.point, Color.yellow);
        }

        // Front mid sensor. If detected obj in front, set flag to true and avoidance to -1 or 1 according the normal of RC hit obj
        // Reset the values set above
        pos = transform.position;
        pos += (transform.forward * frontSensorStartPoint) + transform.up;
        if (avoidSensitivity == 0)
        {
            if (Physics.Raycast(pos, transform.forward, out hit, sensorLenght, layermask))
            {
                // Hit an obstacle, determine the way to evade to by checking the normal between car and the hit.point
                if (hit.normal.x < 0)
                    avoidSensitivity = -1;
                else
                    avoidSensitivity = 1;

                Debug.DrawLine(pos, hit.point, Color.yellow);
            }
        }

        // Start reversing if youve crashed on a wall / slowed down enough and cant dodge object
        //ReversingLogic(avoidSensitivity); // V1
        Reversing(avoidSensitivity); // V2

        // If some obstacles detected, override the steering with avoidSteer with amount of avoidSensitivity
        if (detected)
        {
            AvoidSteer(avoidSensitivity);
        }
    }

    // Try avoid sensed objects by steering away with the amount of avoidance provided
    void AvoidSteer(float sensitivity)
    {
        //wheelFL.steerAngle = avoidSpeed * sensitivity;
        //wheelFR.steerAngle = avoidSpeed * sensitivity;

        wheels[0].steerAngle = avoidSpeed * sensitivity;
        wheels[1].steerAngle = avoidSpeed * sensitivity;
    }
    #region Reversing V1 and V2
    /*    void ReversingLogic(float avoidSensitivity)
        {
            // If car if almost at full (or full) stop and not reversing, then reverse
            if (rb.velocity.magnitude < 0.5f && !reversing)
            {
                // Reset the brakes applied from slowing down on collision
                wheelFL.brakeTorque = 0;
                wheelFR.brakeTorque = 0;

                // Start counter for reversing and set reverse to true
                reverseCounter += Time.deltaTime;
                if (reverseCounter >= waitToReverse)
                {
                    reverseCounter = 0;
                    reversing = true;
                }
            }
            // if is not reversing, set counter to 0
            else if (!reversing)
            {
                reverseCounter = 0;
            }

            // While reversing, set avoid to negative to correct the way to reverse to, reverse for the duration set
            if (reversing)
            {
                avoidSensitivity *= -1;
                reverseCounter += Time.deltaTime;

                if (reverseCounter >= reverseFor)
                {
                    reverseCounter = 0;
                    reversing = false;
                }
            }
        }*/

    void Reversing(float avoidSensitivity)
    {
        // While not yet reversing
        if (rb.velocity.magnitude < 0.5f && !reversing && inReverseRcast)
        {
            // Reset brakes
            //wheelFL.brakeTorque = 0;
            //wheelFR.brakeTorque = 0;
            wheels[0].brakeTorque = 0;
            wheels[1].brakeTorque = 0;
            wheels[2].brakeTorque = 0;
            wheels[3].brakeTorque = 0;

            // Start reversing
            Invoke("StartReverse", waitToReverse);
        }

        // While reversing. Reverse until far enough from object
        if (reversing)
        {
            avoidSensitivity *= -1;

            if (!inReverseRcast)
                reversing = false;
        }
    }

    void StartReverse()
    {
        reversing = true;
    }
    #endregion
    /*
    // Checks and sets next path node target
    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "PathNode")
        {
            if (curNode < nodes.Count-1)
                curNode++;
            else
                curNode = 0;
        }
    }*/

    void CheckDirection()
    {

        // Checking if already going right way, if not then narrow the angle to prevent jamming at 180 angle
        if (!goingRightWay)
            angleOfRightWayDetection = 75;
        else
            angleOfRightWayDetection = 90;
        // If is facing the next node then is heading to right direction
        if (Vector3.Angle(transform.forward, nodes[curNode].position - transform.position) < angleOfRightWayDetection)
        {
            goingRightWay = true;
            //Debug.Log(goingRightWay);
        }

        // Else player is going wrong way
        else
        {
            goingRightWay = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // Car reached intervalTrigger, send car and trigger info to the laptime system
        if (other.gameObject.tag == "IntervalTrigger")
        {
            intervalManager.AddIntervalTime(gameObject, other.gameObject);
        }
    }

    void OnTriggerStay(Collider other)
    {
        // While is braking area, brake if needed
        if (other.tag == "BrakeTrigger")
        {
            isBraking = true;
        }
    }

    void OnTriggerExit(Collider other)
    {// When leaving braking area, stop braking
        if (other.tag == "BrakeTrigger")
        {
            isBraking = false;
        }
    }

    public void SetCarInformation(float posX, float posY, float velX, float velY, float rotZ)
    {
        transform.position = new Vector3(posX, posY, 0);
        transform.rotation = Quaternion.Euler(0, 0, rotZ);
        // Not doing anything with velocity for now
    }
}
