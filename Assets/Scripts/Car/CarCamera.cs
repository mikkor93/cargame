﻿using UnityEngine;
public class CarCamera : MonoBehaviour
{
    Transform carCam;
    Transform car;
    Rigidbody carRb;

    public float rotationThreshold = 1f;
    public float cameraStickiness = 10.0f;
    public float cameraRotationSpeed = 5.0f;

    public float xValue = 0f;
    public float yValue = 3f;
    public float zValue = -8f;

    void Awake()
    {
        carCam = Camera.main.GetComponent<Transform>();
        car = GetComponent<Transform>();
        carRb = car.GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //displaced car position
        Quaternion look;
        // Moves the camera to the cars position.
        Vector3 targetCarPosition = new Vector3(car.position.x + xValue, car.position.y + yValue, car.position.z + zValue);

        // If car isnt moving, look forward by default
        carCam.position = Vector3.Lerp(carCam.position, targetCarPosition, cameraStickiness * Time.fixedDeltaTime);

        if (carRb.velocity.magnitude < rotationThreshold)
            look = Quaternion.LookRotation(car.forward);
        else
            look = Quaternion.LookRotation(car.forward); //Quaternion.LookRotation(carPhysics.velocity.normalized); 

         // Rotate the camera towards the velocity vector. 
        look = Quaternion.Slerp(carCam.rotation, look, cameraRotationSpeed * Time.fixedDeltaTime);
        carCam.rotation = look;
    }
}