﻿using UnityEngine;
using System.Collections;

public class CarCameraV2 : MonoBehaviour {

    public Transform car;
    public bool fovChangesWithSpeed = true;
    public bool reverseCamTurn = true;
    [Range(0, 5)] public float turnTreshold = 0.5f;
    [Range(0, 15)] public float distance = 6f;
    [Range(0, 5)] public float height = 2f;
    [Range(0, 10)] public float rotDamp = 6f;
    [Range(0, 10)] public float heightDamp = 6f;
    [Range(0, 1)] public float zoomRatio = 0.5f;
    [Range(0, 5)] public float fixedLookHeight = 2f;
    [Range(60, 180)] public float DefaultFov = 60f;

    private Vector3 rotationVector;
    private Camera camera;
    private Rigidbody carRb;

    void Start ()
    {
        camera = GetComponent<Camera>();
        carRb = car.GetComponent<Rigidbody>();
    }

    void FixedUpdate ()
    {
        // Find angle and the position of the camera
        float wantedHeight = car.position.y + height;
        float myAngle = transform.localEulerAngles.y;
        float myHeight = transform.position.y;

        myAngle = Mathf.LerpAngle(myAngle, rotationVector.y, rotDamp * Time.deltaTime);
        myHeight = Mathf.Lerp(myHeight, wantedHeight, heightDamp * Time.deltaTime);

        Quaternion currentRotation = Quaternion.Euler(0, myAngle, 0);

        // Set cameras position and make it look at the car
        transform.position = new Vector3(car.position.x, myHeight, car.position.z);
        transform.position -= currentRotation * Vector3.forward * distance;
        Vector3 fixedLookPos = new Vector3(car.transform.position.x, car.transform.position.y + fixedLookHeight, car.transform.position.z);
        transform.LookAt(fixedLookPos);

        // If checked on, when car is reversing -> turn camera to look backwards
        Vector3 localVelocity = car.InverseTransformDirection(carRb.velocity);
        if (localVelocity.z < turnTreshold && reverseCamTurn)
        {
            rotationVector.y = car.eulerAngles.y + 180f;
        }
        else
        {
            rotationVector.y = car.eulerAngles.y;
        }

        // If checked on, change fov with speed
        if (fovChangesWithSpeed)
        {
            float acceleration = car.GetComponent<Rigidbody>().velocity.magnitude;
            camera.fieldOfView = DefaultFov + acceleration * zoomRatio * Time.deltaTime;
        }
    }
}
