﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DamageSystem : MonoBehaviour {

    public Slider S_CarArmor;
    public Text T_DamageText;

    public Manager manager;

    public int CarHealth { get; protected set; }


    private void Start()
    {
        CarHealth = 1;
    }

    public void HitWall () {
        if(CarHealth > 0) {
            S_CarArmor.value--;
            CarHealth--;

            T_DamageText.text = CarHealth.ToString() + " / 10";
        }
        else {
            manager.GameOver();
        }
	}
}
