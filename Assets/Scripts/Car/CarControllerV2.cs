﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//TODO might not work on android
using System.Linq;

public class CarControllerV2 : MonoBehaviour {

    public enum SpeedType {
        KPH,
        MPH
    }

    public WheelCollider[] wheels;
    public GameObject[] tires;
    public GameObject centerOfGravity;
    public WheelEffects[] wheelEffects = new WheelEffects[4];

    [Header("UI")]
    public Text T_SpeedText;
    public Text T_GearText;

    [Header("Car Attributes")]
    public SpeedType speedType;
    [Range(0, 1)]
    public float tractionControl;
    [Range(0, 1)]
    public float steerHelper;
    [Range(0, 3500)]
    public float maxTorque = 1000;
    [Range(100, 1000)]
    public float maxReverseTorque = 500;
    public float maxSteer = 20;
    public float minSteer = 1;
    [Range(100, 750)]
    public float maxSpeed = 150;
    [Range(50, 500)]
    public float downforce = 100;
    [Range(100, 30000)]
    public float antiRoll = 150;
    [Range(10, 100)]
    public float slipLimit = 50;

    public float[] gearRatio;
    public int currentGear;
    [Range(500, 8500)]
    public float maxRPM = 3000f;
    [Range(100, 500)]
    public float minRPM = 1000f;
    public float engineRPM = 0f;
    public float calculatedRPM;
    private int appropriateGear;

    private float maxDrag = 1.0f;
    private float deaccelerationRate = 10;
    private float tyreSlipLimit = 1;
    [SerializeField]
    private float currentSpeed;

    private float dragStartVelocity;
    private float dragMaxVelocity;
    private float maxVelocity;
    private float originalDrag;
    private float sqrDragStartVelocity;
    private float sqrDragVelocityRange;
    private float sqrMaxVelocity;
    private float currentTorque;
    private float oldRotation;
    [SerializeField]
    private float steerAngle;

    Rigidbody rb;
    AudioSource audio;

    public LapTimeSystem intervalManager;
    public CurveHelper curveHelper;

    private float customized_Acceleration;
    private float customized_Brakes;
    private float customized_Grip;
    private float customized_Handling;
    private float customized_Speed;

    private PlayerInput input;
    private DamageSystem damageSystem;

    [SerializeField]
    private static int NoOfGears = 5;
    [SerializeField]
    private float m_RevRangeBoundary = 1f;
    private int m_GearNum;
    private float m_GearFactor;
    public float Revs { get; private set; }


    void Awake() {
        rb = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();

        /*if (intervalManager == null)
            intervalManager = GameObject.Find("IntervalTriggers").GetComponent<LapTimeSystem>();

        if (curveHelper == null)
            curveHelper = GameObject.Find("CurveHelper").GetComponent<CurveHelper>();*/

        dragStartVelocity = maxSpeed / 2;
        dragMaxVelocity = maxSpeed / 2;
        maxVelocity = maxSpeed / 2;
        originalDrag = rb.drag;
        Initialize(dragStartVelocity, dragMaxVelocity, maxSpeed, maxDrag);
    }

    void Initialize(float dragStartVelocity, float dragMaxVelocity, float maxVelocity, float maxDrag) {
        this.dragStartVelocity = dragStartVelocity;
        this.dragMaxVelocity = dragMaxVelocity;
        this.maxVelocity = maxVelocity;
        this.maxDrag = maxDrag;

        sqrDragStartVelocity = dragStartVelocity * dragStartVelocity;
        sqrDragVelocityRange = (dragMaxVelocity * dragMaxVelocity) - sqrDragStartVelocity;
        sqrMaxVelocity = maxVelocity * maxVelocity;

        currentTorque = maxTorque - (tractionControl * maxTorque);
    }

    void Start() {
        input = GetComponent<PlayerInput>();
        damageSystem = GetComponent<DamageSystem>();

        rb.centerOfMass = centerOfGravity.transform.localPosition;

        Invoke("FindTires", .5f);

        CheckUpgades();
    }

    private void GearChanging()
    {
        float f = Mathf.Abs(GetSpeed() / maxSpeed);
        float upgearlimit = (1 / (float)NoOfGears) * (m_GearNum + 1);
        float downgearlimit = (1 / (float)NoOfGears) * m_GearNum;

        if (m_GearNum > 0 && f < downgearlimit)
        {
            m_GearNum--;
            T_GearText.text = "Gear: " +( m_GearNum + 1);
        }

        if (f > upgearlimit && (m_GearNum < (NoOfGears - 1)))
        {
            m_GearNum++;
            T_GearText.text = "Gear: " + (m_GearNum + 1);
        }
    }

    // simple function to add a curved bias towards 1 for a value in the 0-1 range
    private static float CurveFactor(float factor)
    {
        return 1 - (1 - factor) * (1 - factor);
    }

    // unclamped version of Lerp, to allow value to exceed the from-to range
    private static float ULerp(float from, float to, float value)
    {
        return (1.0f - value) * from + value * to;
    }

    private void CalculateGearFactor()
    {
        float f = (1 / (float)NoOfGears);
        // gear factor is a normalised representation of the current speed within the current gear's range of speeds.
        // We smooth towards the 'target' gear factor, so that revs don't instantly snap up or down when changing gear.
        var targetGearFactor = Mathf.InverseLerp(f * m_GearNum, f * (m_GearNum + 1), Mathf.Abs(GetSpeed() / maxSpeed));
        m_GearFactor = Mathf.Lerp(m_GearFactor, targetGearFactor, Time.deltaTime * 5f);
    }

    private void CalculateRevs()
    {
        // calculate engine revs (for display / sound)
        // (this is done in retrospect - revs are not used in force/power calculations)
        CalculateGearFactor();
        var gearNumFactor = m_GearNum / (float)NoOfGears;
        var revsRangeMin = ULerp(0f, m_RevRangeBoundary, CurveFactor(gearNumFactor));
        var revsRangeMax = ULerp(m_RevRangeBoundary, 1f, gearNumFactor);
        Revs = ULerp(revsRangeMin, revsRangeMax, m_GearFactor);
    }

    void FixedUpdate() {
        T_SpeedText.text = "Speed: " + GetSpeed().ToString("000") + " km/h";

#if UNITY_EDITOR
        Drive();
#elif !UNITY_EDITOR
        MobileDrive();
#endif

        CalculateSpeed();
        AllignWheels();

        CalculateRevs();
        GearChanging();
    }

    public float GetSpeed() {
        return rb.velocity.magnitude * 3.6f;
    }

    private void FindTires() {
        //Find the tyres 
        tires = GameObject.FindGameObjectsWithTag("PlayerTire").OrderBy(go => go.name).ToArray();
    }

    private void CheckUpgades() {
        if (DataRepo.hasStartedGameFromSlash)
        {
            Car car = CarDatabse.GetCar(DataRepo.selectedCarID);

            maxSpeed = car.topSpeed;
            //maxSteer += car.handling / 25;
            maxTorque = car.acceleration + 1000;
        }
        else {
            maxTorque = 1500;
            maxSpeed = 450;
        }
    }

    private void CheckForWheelSpin() {
        //Loop through all wheels
        for(int i= 0; i < 4; i++) {
            WheelHit hit;
            wheels[i].GetGroundHit(out hit);

            if (Mathf.Abs(hit.forwardSlip) >= tyreSlipLimit || Mathf.Abs(hit.sidewaysSlip) >= tyreSlipLimit) {
                wheelEffects[i].EmitTyreSmoke();
            }
        }
    }

    private void AllignWheels() {
        for (int i = 0; i < 4; i++) {
            Quaternion quat;
            Vector3 pos;
            wheels[i].GetWorldPose(out pos, out quat);

            //tires[i].position = pos;
            tires[i].transform.rotation = quat;
        }
    }

    private void CapSpeed() {
        switch(speedType) {
            case SpeedType.KPH:
                if (currentSpeed > maxSpeed)
                    rb.velocity = (maxSpeed / 3.6f) * rb.velocity.normalized;
                break;

            case SpeedType.MPH:
                if (currentSpeed > maxSpeed)
                    rb.velocity = (maxSpeed / 2.234f) * rb.velocity.normalized;
                break;
        }
    }

    private void CalculateSpeed() {
        switch(speedType) {
            case SpeedType.KPH:
                currentSpeed = rb.velocity.magnitude * 3.6f;
                break;

            case SpeedType.MPH:
                currentSpeed = rb.velocity.magnitude * 2.234f;
                break;
        }
    }

    private void MobileDrive() {
        //Steering
        wheels[0].steerAngle = steerAngle * input.inputX;
        wheels[1].steerAngle = steerAngle * input.inputX;

        //Throttle
        if (input.throttle) {
            rb.drag = .1f;

            for(int i = 0; i < wheels.Length; i++) {
                wheels[i].motorTorque = currentTorque;
            }
        }
        else {
            rb.drag = .25f;

            for(int i = 0; i < wheels.Length; i++) {
                wheels[i].motorTorque = 0;
            }
        }

        // Brake/Reverse
        if (input.brake) {
            for (int i = 0; i < wheels.Length; i++) {
                wheels[i].motorTorque = -maxReverseTorque;
            }
        }

        SteerHelper();
        CapSpeed();
        ApplyDownForce();
        TractionControl();
        CheckGround();  //TODO change this
        CheckForWheelSpin();
    }

    private void Drive() {
        // Throttle
        if(Input.GetAxis("Vertical") > 0.1f) {
            rb.drag = .1f;

            for (int i = 0; i < wheels.Length; i++)
            {
                wheels[i].motorTorque = Input.GetAxis("Vertical") * currentTorque;
            }
        }

        else {
            rb.drag = .25f;

            for (int i = 0; i < wheels.Length; i++)
            {
                wheels[i].motorTorque = 0;
            }
        }

        wheels[0].steerAngle = steerAngle * Input.GetAxis("Horizontal");
        wheels[1].steerAngle = steerAngle * Input.GetAxis("Horizontal");

        if(Input.GetAxis("Vertical") < 0 && wheels[0].rpm < 0) {
            //Reverse
            wheels[0].motorTorque = -maxReverseTorque;
            wheels[1].motorTorque = -maxReverseTorque;
            wheels[2].motorTorque = -maxReverseTorque;
            wheels[3].motorTorque = -maxReverseTorque;
        }

        SteerHelper();
        CapSpeed();
        ApplyDownForce();
        TractionControl();
        CheckGround();  //TODO change this
        CheckForWheelSpin();
    }

    private void CheckGround() {
        //TODO Change this to Raycast in the future
       WheelHit wheelhit;

       for(int i = 0; i < 4; i++) {
            if(!wheels[i].GetGroundHit(out wheelhit)) {
                Physics.gravity = new Vector3(0, -20f, 0);   
            }
            else {
                Physics.gravity = new Vector3(0, -9.81f, 0);
            }
        }
    }

    private void ApplyDownForce() {
        //Adds more grip in relation to speed
        wheels[0].attachedRigidbody.AddForce(-transform.up * downforce * wheels[0].attachedRigidbody.velocity.magnitude);
    }

    private void TractionControl() {
        WheelHit wheelhit;

        for(int i = 0; i < 4; i++) {
            wheels[i].GetGroundHit(out wheelhit);

            AdjustTorque(wheelhit.forwardSlip);
        }
    }

    private void AdjustTorque(float forwardSlip) { 
        if(forwardSlip >= slipLimit && currentTorque >= 0) {
            currentTorque -= 10 * tractionControl;
        }
        else {
            currentTorque += 10 * tractionControl;

            if(currentTorque > maxTorque) {
                currentTorque = maxTorque;
            }
        }
    }

    private void SteerHelper() {
        //Decrease turn angle depending how fast you are going
        float speedFactor = 1- rb.velocity.magnitude / maxSpeed * 5;
        float currentMaxTurnAngle = ((maxSteer - minSteer) * speedFactor);
        if(currentMaxTurnAngle > minSteer) {
            steerAngle = currentMaxTurnAngle;
        }

        for(int i = 0; i < 4; i++) {
            WheelHit wheelhit;
            wheels[i].GetGroundHit(out wheelhit);
            if(wheelhit.normal == Vector3.zero) {
                //Wheels arent on the normal ground so dont realign the rigidbody velocity
                return;
            }

            //This will prevent gibal lock
            if(Mathf.Abs(oldRotation - transform.eulerAngles.y) < 10f) {
                var turnAdjust = (transform.eulerAngles.y - oldRotation) * steerHelper;
                Quaternion velRot = Quaternion.AngleAxis(turnAdjust, Vector3.up);
                rb.velocity = velRot * rb.velocity;
            }
            oldRotation = transform.eulerAngles.y;
        }
    }

    private void AntiRollBar() {
        WheelHit wheelhit;
        float travelFL = 1.0f;
        float travelFR = 1.0f;
        float travelRL = 1.0f;
        float travelRR = 1.0f;

        var groundedFL = wheels[1].GetGroundHit(out wheelhit);
        if (groundedFL)
            travelFL = (-wheels[1].transform.InverseTransformPoint(wheelhit.point).y - wheels[1].radius) / wheels[1].suspensionDistance;

        var groundedFR = wheels[0].GetGroundHit(out wheelhit);
        if (groundedFR)
            travelFR = (-wheels[0].transform.InverseTransformPoint(wheelhit.point).y - wheels[0].radius) / wheels[0].suspensionDistance;

        var groundedRR = wheels[0].GetGroundHit(out wheelhit);
        if (groundedRR)
            travelRL = (-wheels[2].transform.InverseTransformPoint(wheelhit.point).y - wheels[2].radius) / wheels[2].suspensionDistance;

        var groundedRL = wheels[0].GetGroundHit(out wheelhit);
        if (groundedRL)
            travelRR = (-wheels[0].transform.InverseTransformPoint(wheelhit.point).y - wheels[0].radius) / wheels[0].suspensionDistance;

        var antiRollForce = (travelFL - travelFR - travelRL - travelRR) * antiRoll;

        if (groundedFL)
            rb.AddForceAtPosition(wheels[1].transform.up * -antiRollForce, wheels[1].transform.position);

        if (groundedFR)
            rb.AddForceAtPosition(wheels[0].transform.up * -antiRollForce, wheels[0].transform.position);
    }

#region Triggers
    void OnTriggerEnter(Collider other)
    {
        // Car reached intervalTrigger, send car and trigger info to the laptime system
        if (other.gameObject.tag == "IntervalTrigger")
        {
            if (intervalManager == null)
                intervalManager = GameObject.Find("IntervalTriggers").GetComponent<LapTimeSystem>();

            intervalManager.AddIntervalTime(gameObject, other.gameObject);
        }

        // Car is in curveTrigger, showing the helper UI
        if (other.gameObject.tag == "CurveState1_90left" || other.gameObject.tag == "CurveState2_90right" || other.gameObject.tag == "CurveState3_120left" || other.gameObject.tag == "CurveState4_120right" ||
        other.gameObject.tag == "CurveState5_Sleft" || other.gameObject.tag == "CurveState6_Sright" || other.gameObject.tag == "CurveState7_Uleft" || other.gameObject.tag == "CurveState8_Uright")
        {
            if (curveHelper == null)
                curveHelper = GameObject.Find("CurveHelper").GetComponent<CurveHelper>();

            curveHelper.ShowCurveHelp(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Car exiting curveTrigger, starting fade on UI helper
        {
            if (other.gameObject.tag == "CurveState1_90left" || other.gameObject.tag == "CurveState2_90right" || other.gameObject.tag == "CurveState3_120left" || other.gameObject.tag == "CurveState4_120right" ||
            other.gameObject.tag == "CurveState5_Sleft" || other.gameObject.tag == "CurveState6_Sright" || other.gameObject.tag == "CurveState7_Uleft" || other.gameObject.tag == "CurveState8_Uright")
            {
                curveHelper.FadeHelper();
            }
         }
    }
    #endregion

    #region Collisions
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Wall") {
            //When we hit the wall, decrease the car health by 1
            damageSystem.HitWall();
        }
    }
    #endregion
}
