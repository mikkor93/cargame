﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using System;

public class CarController : MonoBehaviour {

    public bool showDebugGizmos = true;

    public Transform centerOfGravity;
    public List<GameObject> hoverPoints;
    public ParticleSystem[] dustTrails = new ParticleSystem[2];

    public float groundedDrag = 3f;
    public float maxVelocity = 50;
    public float hoverForce = 1000;
    public float gravityForce = 1000f;
    public float hoverHeight = 1.5f;

    public float forwardAcceleration = 8000f;
    public float reverseAcceleration = 4000f;
    public float thrust = 0f;

    public float maxSteer = 25;
    public float minSteer = 3;
    public float turnStrength = 1000f;

    private float turnValue = 0f;
    private float deadZone = 0.1f;
    private int layerMask;
    private Rigidbody rb;
    private PlayerInput input;
    private GameObject[] tires;


    void Start()
    {
        input = GetComponent<PlayerInput>();
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = Vector3.down;

        layerMask = 1 << LayerMask.NameToLayer("Vehicle");
        layerMask = ~layerMask;

        //Find hover points
        for(int i = 0; i < 4; i++) {
            hoverPoints.Add(transform.GetChild(1).transform.GetChild(i).gameObject);
        }

        //Find tires
        Invoke("FindTires", .5f);
    }

    //Visual indication of the raycast hit points in the editor window
    void OnDrawGizmos()
    {
        if(showDebugGizmos) {
            RaycastHit hit;
            for (int i = 0; i < hoverPoints.Count; i++)
            {
                var hoverPoint = hoverPoints[i];
                if (Physics.Raycast(hoverPoint.transform.position,
                                    -Vector3.up, out hit,
                                    hoverHeight,
                                    layerMask))
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawLine(hoverPoint.transform.position, hit.point);
                    Gizmos.DrawSphere(hit.point, 0.5f);
                }
                else
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(hoverPoint.transform.position,
                                    hoverPoint.transform.position - Vector3.up * hoverHeight);
                }
            }
        }
    }

    void Update()
    {
        Inputs();

        //Spin the wheels
        //WheelMovement();
    }

    private void WheelMovement()
    {
        float speed = rb.velocity.magnitude;

        for (int i = 0; i < 4; i++)
        {
            //Rotate all wheels
            float distanceTraveled = speed * Time.deltaTime;
            float rotationInRadians = distanceTraveled / 20;
            float rotationInDegrees = rotationInRadians * Mathf.Rad2Deg;
            tires[i].transform.Rotate(rotationInDegrees * 100, 0, 0);

            //Rotate front wheels
        }
    }

    private void Inputs() {
        //Mobile input
#if !UNITY_EDITOR
        // Get thrust input
        thrust = 0.0f;
        if (input.throttle) {
            thrust = forwardAcceleration;
        }
        else if (input.brake) {
            thrust = -reverseAcceleration;
        }

        if(input.inputX > 0.1f) {
             // Get turning input
            turnValue = 0.0f;
            float turnAxis = input.inputX;
            if (Mathf.Abs(turnAxis) > deadZone)
                turnValue = turnAxis;
        }
#endif

        //In Editor inpput
#if UNITY_EDITOR
        // Get thrust input
        thrust = 0.0f;
        float acceleration = Input.GetAxis("Vertical");
        if (acceleration > deadZone)
            thrust = acceleration * forwardAcceleration;
        else if (acceleration < -deadZone)
            thrust = acceleration * reverseAcceleration;

        if (turnStrength < 50)
        {
            // Get turning input
            turnValue = 0.0f;
            float turnAxis = Input.GetAxis("Horizontal");
            if (Mathf.Abs(turnAxis) > deadZone)
                turnValue = turnAxis;
        }
#endif
    }

    void FixedUpdate()
    {
        //Decrease turn angle depending how fast you are going
        float speedFactor = 1 - rb.velocity.magnitude / maxVelocity * 3; 
        float currentMaxTurnAngle = ((maxSteer - minSteer) * speedFactor);
        if (currentMaxTurnAngle > minSteer)
        {
            turnStrength = currentMaxTurnAngle;
        }

        //  Do hover/bounce force
        RaycastHit hit;
        bool grounded = false;
        for (int i = 0; i < hoverPoints.Count; i++)
        {
            var hoverPoint = hoverPoints[i];
            if (Physics.Raycast(hoverPoint.transform.position, -Vector3.up, out hit, hoverHeight, layerMask))
            {
                rb.AddForceAtPosition(Vector3.up * hoverForce * (1.0f - (hit.distance / hoverHeight)), hoverPoint.transform.position);
                grounded = true;
            }
            else
            {
                // Self levelling - returns the vehicle to horizontal when not grounded and simulates gravity
                if (transform.position.y > hoverPoint.transform.position.y)
                {
                    rb.AddForceAtPosition(hoverPoint.transform.up * gravityForce, hoverPoint.transform.position);
                }
                else
                {
                    rb.AddForceAtPosition(hoverPoint.transform.up * -gravityForce, hoverPoint.transform.position);
                }
            }
        }

        var emissionRate = 0;
        if (grounded)
        {
            rb.drag = groundedDrag;
            emissionRate = 10;
        }
        else
        {
            rb.drag = 0.1f;
            thrust /= 100f;
            turnValue /= 100f;
        }

        if(dustTrails.Length > 0) {
            for (int i = 0; i < dustTrails.Length; i++)
            {
                var emission = dustTrails[i].emission;
                emission.rateOverTime = new ParticleSystem.MinMaxCurve(emissionRate);
            }
        }

        // Handle Forward and Reverse forces
        if (Mathf.Abs(thrust) > 0)
            rb.AddForce(transform.forward * thrust);

        // Handle Turn forces
        if (turnValue > 0)
        {
            rb.AddRelativeTorque(Vector3.up * turnValue * turnStrength);
        }
        else if (turnValue < 0)
        {
            rb.AddRelativeTorque(Vector3.up * turnValue * turnStrength);
        }

        // Limit max velocity
        if (rb.velocity.sqrMagnitude > (rb.velocity.normalized * maxVelocity).sqrMagnitude)
        {
            rb.velocity = rb.velocity.normalized * maxVelocity;
        }
    }

    public float GetSpeed() {
        return rb.velocity.magnitude * 3.6f;
    }

    private void FindTires() {
        tires = GameObject.FindGameObjectsWithTag("PlayerTire");
    }
}
