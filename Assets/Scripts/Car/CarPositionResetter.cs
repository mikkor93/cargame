﻿using UnityEngine;
using System.Collections;

public class CarPositionResetter : MonoBehaviour {

    public Transform[] safePoints;


	void ResetCar() {
        Transform closestSafePoint = null;
        Vector3 currentPos = transform.position;
        float closestDistance = 999999f;

        //Search the closest safePoint
        foreach (Transform trans in safePoints) {
            float pointDistance = Vector3.Distance(currentPos, trans.position);
            if(pointDistance < closestDistance) {
                closestDistance = pointDistance;
                closestSafePoint = trans;
            }
        }

        //Reset the cars position
        transform.position = closestSafePoint.transform.position;
        transform.rotation = closestSafePoint.transform.rotation;
	}

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Killzone") {
            ResetCar();
            Debug.Log("Resetted the car");
        }
    }
}
