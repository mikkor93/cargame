﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathing : MonoBehaviour {

    public bool showGizmos = true;
    public Color lineColor;
    public List<Transform> nodeList = new List<Transform>();

    // Iterate through all pathing nodes and draw lines between them while in editor
    void OnDrawGizmos() // OnDrawGizmosSelected to only show path when "Path" object selected
    {
        if (showGizmos)
        {
            Gizmos.color = lineColor;

            Transform[] pathTransforms = GetComponentsInChildren<Transform>();
            nodeList = new List<Transform>();

            for (int i = 0; i < pathTransforms.Length; i++)
            {
                if (pathTransforms[i] != transform)
                {
                    nodeList.Add(pathTransforms[i]);
                }
            }

            for (int i = 0; i < nodeList.Count; i++)
            {
                Vector3 currentNode = nodeList[i].position;
                Vector3 previousNode = Vector3.zero;

                if (i > 0)
                {
                    previousNode = nodeList[i - 1].position;
                }

                else if (i == 0 && nodeList.Count > 1)
                {
                    previousNode = nodeList[nodeList.Count - 1].position;
                }

                Gizmos.DrawLine(previousNode, currentNode);
                //Gizmos.DrawWireSphere(currentNode, 0.6f);
            }
        }
    }
}
