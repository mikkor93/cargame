﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialRenderOrder : MonoBehaviour {

    public int RenderOrder = 0;


	void Start () {
        GetComponent<Renderer>().material.renderQueue = RenderOrder;
	}
}
