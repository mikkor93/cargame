﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LapTimeSystem : MonoBehaviour {

    // This script should collect
    // 1) ToDo:Intervals for every car, and add all of those to total time and laptimes
    // 2) pop up the previous and next cars times to the player(s)

    [Serializable]
    public class LaptimeHolder
    {
        public GameObject car;
        public int nextInterval = 1;
        public List<float> intervalTime;
        public List<float> lapTime;
    }

    public bool timerRunning;
    public float timer;
    public int amountOfLaps = 4;
    public int intervalsInMap = 4;
    public float timeScale = 1f;
    public List<GameObject> intervals = new List<GameObject>();
    public List<GameObject> cars = new List<GameObject>();
    public List<LaptimeHolder> lapTimes = new List<LaptimeHolder>();

	void Start ()
    {
        Setup();

        timerRunning = true;
    }

    void Update ()
    {
        if(timerRunning)
        {
            timer += Time.deltaTime;
        }

        // For Debugging purposes
        Time.timeScale = timeScale;
    }

    void Setup ()
    {
        // Get the intervals amount in map
        intervals = new List<GameObject>();

        foreach (Transform child in transform)
            intervals.Add(child.gameObject);

        intervalsInMap = intervals.Count;

        // Create and set a custom list for each car in game to hold laptimes in
        cars = new List<GameObject>();
        cars.AddRange(GameObject.FindGameObjectsWithTag("Player"));
        cars.AddRange(GameObject.FindGameObjectsWithTag("Ai Car"));

        for (int i = 0; i < cars.Count; i ++)
        {
            LaptimeHolder curCar = new LaptimeHolder();
            //curCar.car = cars[i].name;
            curCar.car = cars[i];
            curCar.intervalTime = new List<float>();
            curCar.lapTime = new List<float>();
            lapTimes.Add(curCar);
        }
    }

    public void AddIntervalTime (GameObject car, GameObject intervalTrigger)
    {
        // Compare the index of current trigger to the intervals collected, making sure that player wont be able to proc same trigger again and again
        int intervalIndex = intervals.IndexOf(intervalTrigger);
        int index = cars.IndexOf(car);

        // If nextInterval is same as the amount of intervals in map, reset it to 0 (and collect data for full lap)
        if (lapTimes[index].nextInterval == intervalsInMap)
        {
            lapTimes[index].intervalTime.Add(timer);
            lapTimes[index].lapTime.Add(timer);

            lapTimes[index].nextInterval = 1;
            Debug.Log("Full lap!"); 
        }

        // If hit interval is the assigned "nextInterval", then car has reached the correct interval
        else if (lapTimes[index].nextInterval == intervalIndex)
        {
            lapTimes[index].intervalTime.Add(timer);
            lapTimes[index].nextInterval++;
        }

          /*  for (int i = 1; i < amountOfLaps; i++)
            {
                Debug.Log("3");
                // Check if full lap has been done, if has, then add this laps intervals into laptime
                if (lapTimes[index].intervalTime.Count == intervalsInMap * i)
                {
                    Debug.Log("Lap " + i);
                    float lapTime = lapTimes[index].intervalTime[(intervalsInMap * i) - 1];
                    lapTimes[index].lapTime.Add(lapTime);
                }
            }*/
    }

    void OnGUI ()
    {
        GUILayout.Label(" Total time: " + timer.ToString("F2"));
    }
}
/*
 *         if (lapTimes[index].lastInterval + 1 == intervalIndex)// eli.. jos osuttu intervalli on lastInterval +1 kyseisen auton kohdalla
        {
            // Add interval time to the correct car
            lapTimes[index].intervalTime.Add(timer);

                Debug.Log("if");
                lapTimes[index].lastInterval++;
        }
        else if (lapTimes[index].lastInterval == intervalIndex)
        {
            if (lapTimes[index].hasPassedStartLine)
            {
                Debug.Log("Full lap!");
                lapTimes[index].intervalTime.Add(timer);
                lapTimes[index].lastInterval = 0;
            }
            else
            {
                Debug.Log("Passed start line");
                lapTimes[index].hasPassedStartLine = true;
            }
        }

 */
