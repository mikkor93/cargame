﻿/*using UnityEngine;    Disabled for debugging
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames.BasicApi.Multiplayer;

public class GameManager : MonoBehaviour, MPUpdateListener
{
    public GameObject myCar;
    public GameObject opponentPrefab;

    private bool multiplayerReady;
    private string myParticipantId;
    private Vector2 startingPoint = new Vector2(1f, -2f);
    private float startingPointYOffset = 0.2f;
    private Dictionary<string, NetworkPosTracking> opponentScripts;

    //private Rigidbody rb;

	void SetupMultiplayerGame ()
    {
        MultiplayerController.Instance.updateListener = this;

        // Gets the local player’s participant ID and stores it as a local variable.
        myParticipantId = MultiplayerController.Instance.GetMyParticipantId();

        // Grabs the list of sorted participants from the multiplayer controller so you can iterate through them.
        List<Participant> allPlayers = MultiplayerController.Instance.GetAllPlayers();
        opponentScripts = new Dictionary<string, NetworkPosTracking>(allPlayers.Count - 1);

        for(int i = 0; i < allPlayers.Count; i++)
        {
            string nextParticipantId = allPlayers[i].ParticipantId;
            Debug.Log("Setting up car for " + nextParticipantId);
            // Calculates the start point for each car based on its order in the list. //ToDo this
            Vector3 carStartPoint = new Vector3(startingPoint.x, startingPoint.y + (i * startingPointYOffset), 0);

            // If the participantID of the current player matches the local player, 
            // then instruct the CarController script to set the car number, which determines its color.
            if (nextParticipantId == myParticipantId) //ToDo this
            {
                //myCar.GetComponent<CarController>().SetCarChoise(i+1, true);
                //myCar.transform.position = carStartPoint;
            }

            //Otherwise, instantiate a new opponent car from the Prefab object and set its car number and color as well.
            else
            {
                GameObject opponentCar = (Instantiate(opponentPrefab, carStartPoint, Quaternion.identity) as GameObject);
                NetworkPosTracking opponentScript = opponentCar.GetComponent<NetworkPosTracking>();
                // opponentScript.SetCarNumber(i+1);

                // You’re storing this carController in a dictionary with the participant ID as the key. Since you’ll be 
                // getting a lot of messages in the form “Participant ID xxx said so-and-so”, storing your opponents in a 
                // dictionary is an easy way to refer to them later by their participant ID.
                opponentScripts[nextParticipantId] = opponentScript;
            }
        }

        //ToDo Could set the UIs laps etc here
        //lapsRemaining = 3;
        //timePlayed = 0;
        //guiObject.SetLaps(_lapsRemaining);
        //guiObject.SetTime(_timePlayed);
        multiplayerReady = true;

        InvokeRepeating("DebuggingSpamaroni", 1, 1);
    }

    void DoMultiplayerUpdate ()
    {
        MultiplayerController.Instance.SendMyUpdate(myCar.transform.position.x, myCar.transform.position.y, myCar.GetComponent<Rigidbody>().velocity, myCar.transform.rotation.eulerAngles.z);
    }

    public void UpdateReceived (string senderId, float posX, float posY, float velX, float velY, float rotZ)
    {
        if (multiplayerReady)
        {
            NetworkPosTracking opponent = opponentScripts[senderId];

            if (opponent != null)
            {
                opponent.SetCarInformation(posX, posY, velX, velY, rotZ); //SetCarInfo is implemented in AI car atm
            }
        }
    }

    void DebuggingSpamaroni ()
    {
        Debug.Log(MultiplayerController.Instance.GetAllPlayers());
    }
}*/
