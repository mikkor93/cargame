﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MinimapManager : MonoBehaviour {
    [Header("Drag n drop from prefabs > UI > Minimap Visuals")]
    public GameObject playersVisual;
    public GameObject aiVisual;
    //public Quaternion visualRot;

	void Start ()
    {
        MarkForMinimap();
	}

    void MarkForMinimap ()
    {
        // Find and add visuals for minimap on players
        List<GameObject> players = new List<GameObject>();
        players.AddRange (GameObject.FindGameObjectsWithTag("Player"));

        for (int i = 0; i < players.Count; i++)
        {
            GameObject visual = Instantiate(playersVisual, players[i].transform.position, aiVisual.transform.rotation) as GameObject;
            visual.transform.parent = players[i].transform;
            //visual.transform.rotation = visualRot;
        }

        // Find and add visuals for minimap on AI cars
        List<GameObject> aiCars = new List<GameObject>();
        aiCars.AddRange(GameObject.FindGameObjectsWithTag("Ai Car"));

        for (int i = 0; i < aiCars.Count; i ++)
        {
            GameObject visual = Instantiate(aiVisual, aiCars[i].transform.position, aiVisual.transform.rotation) as GameObject;
            visual.transform.parent = aiCars[i].transform;
            //visual.transform.rotation = visualRot;
        }
    }
}
