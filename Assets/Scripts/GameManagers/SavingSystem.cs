﻿using UnityEngine;
using System.Collections;

public class SavingSystem : MonoBehaviour {

    public string nameOfSaveFile = "1";
    public string defaultDataString1 = "EnterEmptyDataTemplateHere";
    public string defaultDataString2 = "EnterEmptyDataTemplateHere";
    // Saving system that should save datas in arrays and such.
    // Saveable datas;
    // (Player name), current settings, cur car, (cur skin, cur point allocated on the car)
    // Array for each track, holding 1) All best interval times. 2) Best lap time ( 3) The car that the best lap was made with, possibly even skins etc?))
    // Array that would hold all the cars. First slots would be holding strings that can be chopped down to get the "customized points" and/or skin

    // So, 2 string arrays. First would be "loadedData" that would hold:
    // string1 current settings, current car, all the openable skins if open or not " 00010101" way
    // string2 -> string for each car. The string would contain the possible points allocated in them and the current skin on it

    // Stringarray2 would contain the interval-/laptimes for each track


    void Start ()
    {
	
	}
	
    void LoadAndReadData()
    {
        string[] loadedData = PlayerPrefsX.GetStringArray(nameOfSaveFile);

        if(loadedData.Length == 0)
        {
            loadedData = new string[50];
            Debug.Log("Running game for the first time!");
        }

        // = = = = = = = = = = STRING 1 = = = = = = = = = =
        // If theres no data, create a new one
        if (loadedData[0] == "" || loadedData[0] == null)
            loadedData[0] = defaultDataString1;

        // = = = SETTINGS DATA = = =

        /*
         *         // Music volume
        musicVolumeString = loadedData[4].Substring(7, 3);
        float.TryParse(musicVolumeString, out musicVolume);
        musicVolume = musicVolume - 80f;
        master.SetFloat("VolumeParameter", musicVolume);

        //SFX Volume
        SfxVolumeString = loadedData[4].Substring(10, 3);
        float.TryParse(SfxVolumeString, out sfxVolume);
        sfxVolume = sfxVolume - 80f;
        master.SetFloat("SFXParameter", sfxVolume);

        // Mute Toggle
        soundsEnabledString = loadedData[4].Substring(13, 1);
        if (soundsEnabledString == "0")
            master.SetFloat("MainVolume", -5f);
        //master.SetFloat("MainVolume", startMasterVolume);
        else if (soundsEnabledString == "1")
            master.SetFloat("MainVolume", -80f);

        // Vibration Toggle
        vibrateEnabledString = loadedData[4].Substring(14, 1);
        if (vibrateEnabledString == "0")
            DataRepo.canVibrate = true;
        else if (vibrateEnabledString == "1")
            DataRepo.canVibrate = false;
         */

        //curThrustersString = loadedData[4].Substring(4, 1); for possible use as "getting cur car"
        //Int32.TryParse(curThrustersString, out curThrusters);

        // = = = = = = = = = = STRING 2 - 50 = = = = = = = = = =
        // If theres no data, create a new one
        if (loadedData[1] == "" || loadedData[1] == null)
            loadedData[1] = defaultDataString2;

        for(int i = 1; i < loadedData.Length; i++)
        {
            //loadedData[i].Substring(10, 3); Get the ints multiplying with the i gotten by the for loop
        }
    }

}
