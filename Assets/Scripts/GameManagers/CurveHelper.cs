﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class CurveHelper : MonoBehaviour {

    public float fadeoutTime = 3f;
    public Image curveHelperImage;
    public Image curveHelperImageWrongWay;
    public RectTransform curveHelperTransform;
    public Sprite[] curveArrows;
    public Vector3[] helperTransforms;
    public GameObject player;
    public bool goingRightWay;
    public float angleOfRightWayDetection = 90f;
    public float distFromNode = 20f;
    public Transform path;
    private List<Transform> pathNodes;
    public int curNode = 0;

    void Start ()
    {
        if (curveHelperImage == null)
            curveHelperImage = GameObject.Find("CurveHelperImage").GetComponent<Image>();

        if (curveHelperTransform == null)
            curveHelperTransform = GameObject.Find("CurveHelperImage").GetComponent<RectTransform>();

        if (curveHelperImageWrongWay == null)
            curveHelperImageWrongWay = GameObject.Find("CurveHelperImageWrongWay").GetComponent<Image>();

        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");

        if (path == null)
            path = GameObject.Find("Path").GetComponent<Transform>();

        GetPath();

        // Hide the helper on start
        curveHelperImage.CrossFadeAlpha(0f, 0f, true);
        curveHelperImageWrongWay.CrossFadeAlpha(0f, 0f, true);

        InvokeRepeating("CheckNodeDistance", 0f, 0.1f);
        InvokeRepeating("CheckDirection", 0f, 1f);
    }
	
    // States in ints. Shows curve UI according to them
    // 1. 90 turn left  2. 90 turn right  3. 120 turn left  4. 120 turn right
    // 5. S turn left  6. S turn right  7. U turn left  8. U turn right
    public void ShowCurveHelp (GameObject trigger)
    {
        if (goingRightWay)
        {
            curveHelperImage.CrossFadeAlpha(1f, 0.1f, true);
            // Show the image to match the incoming curve. Using 4 sprites and mirrorin them to get both right/left
            if (trigger.tag == "CurveState1_90left")
            {
                curveHelperImage.sprite = curveArrows[0];
                curveHelperTransform.localScale = helperTransforms[1];
            }

            if (trigger.tag == "CurveState2_90right")
            {
                curveHelperImage.sprite = curveArrows[0];
                curveHelperTransform.localScale = helperTransforms[0];
            }

            if (trigger.tag == "CurveState3_120left")
            {
                curveHelperImage.sprite = curveArrows[1];
                curveHelperTransform.localScale = helperTransforms[1];
            }

            if (trigger.tag == "CurveState4_120right")
            {
                curveHelperImage.sprite = curveArrows[1];
                curveHelperTransform.localScale = helperTransforms[0];
            }

            if (trigger.tag == "CurveState5_Sleft")
            {
                curveHelperImage.sprite = curveArrows[2];
                curveHelperTransform.localScale = helperTransforms[1];
            }

            if (trigger.tag == "CurveState6_Sright")
            {
                curveHelperImage.sprite = curveArrows[2];
                curveHelperTransform.localScale = helperTransforms[0];
            }

            if (trigger.tag == "CurveState7_Uleft")
            {
                curveHelperImage.sprite = curveArrows[3];
                curveHelperTransform.localScale = helperTransforms[1];
            }

            if (trigger.tag == "CurveState8_Uright")
            {
                curveHelperImage.sprite = curveArrows[3];
                curveHelperTransform.localScale = helperTransforms[0];
            }
        }
    }

    void CheckNodeDistance() // Also could be update if player is passing nodes with this invokeRepeating
    {
        Vector3 relativeVector = player.transform.InverseTransformPoint(pathNodes[curNode].position);

        if (relativeVector.magnitude <= distFromNode)
        {
            if (curNode < pathNodes.Count - 1)
                curNode++;
            else
                curNode = 0;
        }
    }

    void CheckDirection()
    {
        // Todo: If player goes far enough backwards itll enable false "the right way" again as player will end up facing the next node
        // If player is facing the next node (taken from ai pathing) then player is heading to right direction
        if (Vector3.Angle(player.transform.forward, pathNodes[curNode].position - player.transform.position) < angleOfRightWayDetection)
        {
            goingRightWay = true;
            curveHelperImage.enabled = true;
            curveHelperImageWrongWay.CrossFadeAlpha(0f, 0f, true);
        }

        // Else player is going wrong way, hide arrow helper and show wrong way icon
        else
        {
            goingRightWay = false;
            curveHelperImage.enabled = false;
            curveHelperImageWrongWay.CrossFadeAlpha(1f, 0.1f, true);
        }
    }

    public void FadeHelper ()
    {
        curveHelperImage.CrossFadeAlpha(0f, fadeoutTime, true);
        //Debug.Log("fading"); // Todo: This triggers 6 times on trigger enter??
    }

    #region GetPath
    void GetPath()
    {
        // Get the path transforms
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        pathNodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                pathNodes.Add(pathTransforms[i]);
            }
        }
    }
    #endregion
}
