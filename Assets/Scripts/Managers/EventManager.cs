﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using System;

public class EventManager : MonoBehaviour {

    //in here we call the events

    public delegate void AchievementEventHandler();

    public static event AchievementEventHandler OnSpeedReached;

    public bool[] hasAchievements;

    public string[] achievementIDs;


    private void Start()
    {
        CheckGottenAchievements();
    }

    private void CheckGottenAchievements()
    {
        Social.LoadAchievements(achievements => {
            if(achievements.Length > 0) {
                Debug.Log("Number of achievements " + achievements.Length);

                foreach(IAchievement achievement in achievements) {
                    CheckAchievement(achievementIDs[0]);
                }
            }
        });
    }

    private void CheckAchievement(string ID) {
        //Loop through all the achievement IDs
        for(int i = 0; i > achievementIDs.Length; i++) {
            //if the passed ID matches the ID in the achievementIDs
            if(achievementIDs[i] == ID) {
                hasAchievements[i] = true;
            }
        }
    }

    /*public static void GiveSpeedAchievement() {
        if(OnSpeedReached != null && !hasSpeedAchievement) {
            OnSpeedReached();
            hasSpeedAchievement = true;
        }
    }*/
}
