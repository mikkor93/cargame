﻿using UnityEngine;
using System.Collections;
using System;

public class DataRepo : MonoBehaviour {

    //Upgraded car stas
    public int car__Upgraded_Acceleration;
    public int car__Upgraded_Brakes;
    public int car_Upgraded_Handling;
    public int car_Upgraded_TopSpeed;

    static public bool hasStartedGameFromSlash;

    static public int selectedCarID = -1;

    // Saved variables
    static public int controlSchemeID = -1;
    static public int graphicsLevel;


    void Awake () {
        hasStartedGameFromSlash = true;
        Debug.LogWarning("Game started from the splash screen");

        //TODO relocate this to saving system
        CheckForSavedVariables();

        DontDestroyOnLoad(this);
    }

    private void CheckForSavedVariables()
    {
        controlSchemeID = PlayerPrefs.GetInt("ControlSchemeID");

    }
}
