﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {

    public GameObject[] UI;


	public void GameOver () {
        UI[0].SetActive(true);
        UI[1].SetActive(false);
    }

    public void OnGameOverButtonPressed(int sceneID) {
        SceneManager.LoadScene(sceneID);
    }
}
