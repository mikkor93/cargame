﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CarSelectMenu : MonoBehaviour {

    public Slider[] SL_CarAttributes;
    public Text T_CarName;

    public GameObject[] carMeshes;

    public int selectedCarIndex = 0;


    void Start () {
        UpdateCarAttributesUI(0);
    }

    public void Swipe_Right() {
        if(selectedCarIndex < carMeshes.Length - 1) {
            selectedCarIndex++;

            for (int i = 0; i < carMeshes.Length; i++) {
                if(i == selectedCarIndex) {
                    carMeshes[i].SetActive(true);
                    UpdateCarAttributesUI(i);
                }
                else {
                    carMeshes[i].SetActive(false);
                }
            }
        }
    }

     public void Swipe_Left() {
         if(selectedCarIndex > 0) {
            selectedCarIndex--;

            for (int i = 0; i < carMeshes.Length; i++) {
                if(i == selectedCarIndex) {
                    carMeshes[i].SetActive(true);
                    UpdateCarAttributesUI(i);
                }
                else {
                    carMeshes[i].SetActive(false);
                }
            }
        }
    }

    //TODO move this to MainMenuManager when it's done
    public void StartRace() {
        Application.LoadLevel(1);
    }

    public void SelectCar() {
        DataRepo.selectedCarID = selectedCarIndex;
    }

    private void UpdateCarAttributesUI(int carIndex) {
        Car car = CarDatabse.GetCar(carIndex);

        T_CarName.text = car.carName;
        SL_CarAttributes[0].value = car.acceleration;
        SL_CarAttributes[1].value = car.brakes;
        SL_CarAttributes[2].value = car.handling;
        SL_CarAttributes[3].value = car.topSpeed;
    }
}
