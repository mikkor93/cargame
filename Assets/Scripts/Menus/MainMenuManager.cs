﻿using UnityEngine;
using System.Collections;

public class MainMenuManager : MonoBehaviour, MPLobbyListener{

    void Start ()
    {
        //MultiplayerController.Instance.TrySilentSignIn(); disabled for debugging
    }

    public void RaceMode_SingleRace() {

    }

    public void RaceMode_Championship() {

    }

    public void RaceMode_TimeAttack() {

    }

    public void RaceMode_OnlineRace() {

    }

    public void QuitGame() {

    }

    //
    public void StartSinglePlayer ()
    {

    }

    public void StartMultiplayer ()
    {
        //MultiplayerController.Instance.SignInAndStartMPGame(); disabled for debugging
    }

    public void SignOut ()
    {
        //if(MultiplayerController.Instance.IsAuthenticated()) disabled for debugging
        //    MultiplayerController.Instance.SignOut();
    }

    //public GUISkin guiSkin;
    //private bool _showLobbyDialog;
    private string lobbyMessage = "empty";

    public void SetLobbyStatusMessage (string message)
    {
        lobbyMessage = message;
    }

    public void HideLobby()
    {
        //lobbyMessage = "";
        //_showLobbyDialog = false;
    }

    void OnGUI ()
    {
        GUILayout.Label(lobbyMessage);
    }
    //
}
