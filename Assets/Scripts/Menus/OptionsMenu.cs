﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {

    public Dropdown controlScheme;
    public Dropdown graphicsOptions;


    private void Start() {
        controlScheme.value = DataRepo.controlSchemeID;
        graphicsOptions.value = QualitySettings.GetQualityLevel();
    }

    public void ChangeControlScheme() {
        DataRepo.controlSchemeID = controlScheme.value;

        //TODO relocate this to saving system
        PlayerPrefs.SetInt("ControlSchemeID", controlScheme.value);

        Debug.Log("Control SchemeID " + DataRepo.controlSchemeID);
    }

    public void ChangeGraphicsQuality() {
        DataRepo.graphicsLevel = graphicsOptions.value;
        QualitySettings.SetQualityLevel(DataRepo.graphicsLevel);

        Debug.Log("Graphics option: " + DataRepo.graphicsLevel);
    } 
}
