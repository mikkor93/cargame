﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CarCustomizeMenu : MonoBehaviour {

    public Image[] statStarFills;
    public Text[] statTexts;
    public Text upgradePointsText;

    public int upgradePoints = 100;
    private int acceleration = 0;
    private int handling = 0;
    private int speed = 0;

    DataRepo dataRepo;

    //TODO DELETE
    public void LoadNextLevel() {
        Application.LoadLevel(2);
    }

    void Start() {
        if (dataRepo == null)
            dataRepo = GameObject.Find("DataRepo").GetComponent<DataRepo>();

        UpdateUIToCurrentCar();
        UpdateStatTexts();
    }

    private void UpdateStatTexts() {
        //Stat texts
        statTexts[0].text = acceleration.ToString("000");
        statTexts[3].text = handling.ToString("000");
        statTexts[4].text = speed.ToString("000");

        //Stat star fills
        statStarFills[0].fillAmount = handling / 100f;
        statStarFills[2].fillAmount = speed / 100f;
        statStarFills[3].fillAmount = acceleration / 100f;

        //Update updatePoints
        upgradePointsText.text = upgradePoints.ToString("000");
    }

    public void UpdateUIToCurrentCar() {
        Car car = CarDatabse.GetCar(DataRepo.selectedCarID);
        acceleration = (int)car.acceleration;
        handling = (int)car.handling;
        speed = (int)car.topSpeed / 100;
    }

    public void SaveCustomizeValues() {
        //Saves the values to DataRepo
        dataRepo.car__Upgraded_Acceleration = acceleration;
        dataRepo.car_Upgraded_Handling = handling;
        dataRepo.car_Upgraded_TopSpeed = speed;
    }

    public void LoadCustomizedValues() {
        //Load customized values from a save file

    }

    #region Customize buttons
    public void Increase_Acceleration() {
        if(acceleration < 100 && upgradePoints > 0) {
            acceleration++;
            upgradePoints--;
            UpdateStatTexts();
        }
    }

    public void Decrease_Acceleration() {
        if(acceleration > 0) {
            acceleration--;
            upgradePoints++;
            UpdateStatTexts();
        }
    }

    public void Increase_Handling() {
        if(handling < 100 && upgradePoints > 0) {
            handling++;
            upgradePoints--;
            UpdateStatTexts();
        }
    }

    public void Decrease_Handing() {
        if(handling > 0) {
            handling--;
            upgradePoints++;
            UpdateStatTexts();
        }
    }

    public void Increase_Speed() {
        if(speed < 100 && upgradePoints > 0) {
            speed++;
            upgradePoints--;
            UpdateStatTexts();
        }
    }

    public void Decrease_Speed() {
         if(speed > 0) {
            speed--;
            upgradePoints++;
            UpdateStatTexts();
        }
    }
    #endregion
}
