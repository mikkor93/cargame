﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Track {
    public string name;
    public int ID;
    public Sprite trackThumbnail;
    public bool isUnlocked;
    public Button.ButtonClickedEvent onClick;
}

public class LevelViewFill : MonoBehaviour {

    public GameObject trackButtonPrefab;
    public int trackCount = 8;
    public Transform anchorTransform;
    public float right;
    public float height;

    private int trackIndex = 0;

    public List<Track> tracks;


	void Start () {
        PopulateList();
    }

    private void PopulateList() {
        RectTransform rect = anchorTransform.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(((float)trackCount * 650) + 150 * trackCount - 2, height);

        foreach (var track in tracks) {
            GameObject go = Instantiate(trackButtonPrefab);
            go.transform.SetParent(anchorTransform, false);
            Button trackButton = go.GetComponent<Button>();
            trackButton.onClick = track.onClick;
            trackButton.transform.GetChild(0).GetComponent<Text>().text = track.name;
            trackButton.transform.GetChild(1).GetComponent<Image>().sprite = track.trackThumbnail;

            if (track.isUnlocked) {
                trackButton.interactable = true;
            }
            else {
                trackButton.interactable = false;
            }
        }
    }

    public void TrackSelected(int trackIndex) {
        Application.LoadLevel(trackIndex);
    }
}
