﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSound : MonoBehaviour {

    public AudioClip S_Button;
    public AudioClip S_StatIncrease;
    public AudioClip S_StatDecrease;
    public AudioClip S_Error;
    public AudioClip S_Select;

    private AudioSource audio;


    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    public void PlaySoundEffect_Button() {
        audio.PlayOneShot(S_Button);
    }

    public void PlaySoundEffect_StatIncease() {
        audio.PlayOneShot(S_StatIncrease);
    }

    public void PlaySoundEffect_StatDecrease() {
        audio.PlayOneShot(S_StatDecrease);
    }

    public void PlaySoundEffect_Error() {
        audio.PlayOneShot(S_Error);
    }

    public void PlaySoundEffect_Select() {
        audio.PlayOneShot(S_Select);
    }
}
