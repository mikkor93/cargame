﻿using UnityEngine;
using System.Collections;

public class NetworkPosTracking : MonoBehaviour {

    public void SetCarInformation(float posX, float posY, float velX, float velY, float rotZ)
    {
        transform.position = new Vector3(posX, posY, 0);
        transform.rotation = Quaternion.Euler(0, 0, rotZ);
        // Not doing anything with velocity for now
    }
}
