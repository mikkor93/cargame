﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Speedometer : MonoBehaviour {

    public CarControllerV2 carController;

    public Text carMaxSpeedText;
    public Text currentSpeedText;

    private Slider speedometerSlider;
    private float carSpeed;


	void Start () {
        speedometerSlider = GetComponent<Slider>();

        speedometerSlider.maxValue = carController.maxSpeed;
        carMaxSpeedText.text = carController.maxSpeed.ToString();

    }
	
	void Update () {
        CalculateSpeed();
    }

    
    private void CalculateSpeed() {
        //carSpeed = carController.currentSpeed;

        switch (carController.speedType) {
            case CarControllerV2.SpeedType.KPH:
                currentSpeedText.text = carSpeed.ToString("000");
                break;

            case CarControllerV2.SpeedType.MPH:
                break;
        }

        speedometerSlider.value = Mathf.Round(carSpeed);
    }
}
