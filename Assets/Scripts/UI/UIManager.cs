﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public GameObject pauseWindow;
    public string levelNameToLoad = "Demo";


    private void Update() {
        if(Input.GetKeyDown(KeyCode.Escape) && !pauseWindow.activeInHierarchy) {
            pauseWindow.SetActive(true);
        }
        else if(Input.GetKeyDown(KeyCode.Escape) && pauseWindow.activeInHierarchy) {
            pauseWindow.SetActive(false);
        }
    }

    public void PauseGame() {
        pauseWindow.SetActive(true);
    }

    public void LoadScene(int levelID) {
        SceneManager.LoadScene(levelID);
    }

    public void QuitGame () {
        Application.Quit();
	}
}
