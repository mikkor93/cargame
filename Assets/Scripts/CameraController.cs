﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float speed;
    public Vector3 offset;


	void Update () {
        transform.position = Vector3.Lerp(transform.position + offset, target.position, Time.deltaTime * speed);
	}
}
