﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ResetPlayerPrefsMenu : MonoBehaviour {

    [MenuItem("Edit/Reset PlayerPrefs")]
    public static void ResetPlayerPrefs ()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("PlayerPrefs Reset");
    }
}
