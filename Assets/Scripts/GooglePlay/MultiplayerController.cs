﻿/*using UnityEngine;    Disabled for debugging
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;

public class MultiplayerController : RealTimeMultiplayerListener
{
    public MPLobbyListener lobbyListener;
    public MPUpdateListener updateListener;
    private byte protocolVersion = 1;
    // Byte + Byte + 2 floats for position + 2 floats for velocity + 1 float for rotz
    private int updateMessageLength = 22;
    private List<byte> updateMessage;

    // GP Login

    private static MultiplayerController _instance = null;

    private MultiplayerController()
    {
        updateMessage = new List<byte>(updateMessageLength);

        //>>>
        Social.localUser.Authenticate((bool success) => {
            if (success)
            {
                Debug.Log("You've successfully logged in");
            }
            else
            {
                Debug.Log("Login failed for some reason");
            }
        });

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        PlayGamesPlatform.InitializeInstance(config);
        //<<< This is probably not needed

        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }
    

    public static MultiplayerController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new MultiplayerController();
            }
            return _instance;
        }
    }

    public void SignInAndStartMPGame()
    {
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
                    Debug.Log("Succesfully signed in as " + PlayGamesPlatform.Instance.localUser.userName);
                    // Start game
                    StartMatchmaking();
                }
                else
                {
                    Debug.Log("Did not sign in..");
                }
            });
        }
        else
        {
            Debug.Log("Already logged in");
            // Start game
            StartMatchmaking();
        }
    }

    public void TrySilentSignIn()
    {
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.Authenticate((bool success) =>
           {
               if (success)
               {
                   Debug.Log("Silently logged in as " + PlayGamesPlatform.Instance.localUser.userName);
               }
               else
               {
                   Debug.Log("Did not sign in..");

               }
           }, true);
        }
        else
        {
            Debug.Log("Already logged in");
        }
    }

    public void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    public bool IsAuthenticated()
    {
        return PlayGamesPlatform.Instance.localUser.authenticated;
    }

    // Room 

    private uint minOpponents = 1;
    private uint maxOpponents = 4;
    private uint gameVariation = 0;

    private void StartMatchmaking()
    {
        PlayGamesPlatform.Instance.RealTime.CreateQuickGame(minOpponents, maxOpponents, gameVariation, this);
    }

    private void ShowMPStatus (string message)
    {
        Debug.Log(message);
     //   if (lobbyListener == null)
     //       lobbyListener = GameObject.Find("MainMenu").GetComponent<MainMenuManager>();
        if (lobbyListener != null)
            lobbyListener.SetLobbyStatusMessage(message);
    }

    public void OnRoomSetupProgress(float percent)
    {
        ShowMPStatus("Setup is " + percent + "% done");
    }

    public void OnRoomConnected(bool success)
    {
        if (success)
        {
            ShowMPStatus("Succesfully connected to a room, can now start the game");
            //lobbyListener.HideLobby();
            lobbyListener = null;
            Application.LoadLevel("Track1Testing");
        }
        else
            ShowMPStatus("Failed to connect the room");
    }

    public void OnLeftRoom()
    {
        ShowMPStatus("Left the room, prolly should do some clean up tasks");
    }

    public void OnPeersConnected(string[] participantsIds)
    {
        foreach (string participantID in participantsIds)
        {
            ShowMPStatus("Player " + participantID + " has joined");
        }
    }

    public void OnPeersDisconnected(string[] participantsIds)
    {
        foreach (string participantID in participantsIds)
        {
            ShowMPStatus("Player " + participantID + " has left");
        }
    }

    public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
        // we'll be doing more with this later..
        byte messageVersion = (byte)data[0];

        // Figure out what type of message this is
        char messageType = (char)data[1]; // As in what letter is on place 2 of the "string"
        if(messageType == 'U' && data.Length == updateMessageLength)
        {
            float posX = System.BitConverter.ToSingle(data, 2);
            float posY = System.BitConverter.ToSingle(data, 6);
            float velX = System.BitConverter.ToSingle(data, 10);
            float velY = System.BitConverter.ToSingle(data, 14);
            float rotZ = System.BitConverter.ToSingle(data, 18);
            Debug.Log("Player " + senderId + " is at (" + posX + ", " + posY + ") traveling (" + velX + ", " + velY + ") rotation " + rotZ);
               
            if(updateListener != null)
            {
                updateListener.UpdateReceived(senderId, posX, posY, velX, velY, rotZ);
            }
        }
    }

    public void OnParticipantLeft (Participant leavingPlayer)// Stub
    {
        Debug.Log("Player has left");
        // Stub. you could probably use this to make sure that your lobby display doesn't get 
        //messed up if a player joins, then leaves while you're waiting for a third or fourth player to join.
    }

    public List<Participant> GetAllPlayers ()
    {
        return PlayGamesPlatform.Instance.RealTime.GetConnectedParticipants();
    }

    public string GetMyParticipantId ()
    {
        return PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;
    }

    public void SendMyUpdate(float posX, float posY, Vector2 velocity, float rotZ)
    {
        updateMessage.Clear();
        updateMessage.Add(protocolVersion);
        updateMessage.Add((byte)'U'); // Added U to start to stand as Update (could be C for chat and so on)
        updateMessage.AddRange(System.BitConverter.GetBytes(posX));
        updateMessage.AddRange(System.BitConverter.GetBytes(posY));
        updateMessage.AddRange(System.BitConverter.GetBytes(velocity.x));
        updateMessage.AddRange(System.BitConverter.GetBytes(velocity.y));
        updateMessage.AddRange(System.BitConverter.GetBytes(rotZ));

        byte[] messageToSend = updateMessage.ToArray();
        //Debug.Log("Sending my update message " + messageToSend + " to all players in the room");
        PlayGamesPlatform.Instance.RealTime.SendMessageToAll(false, messageToSend);
    }

}
*/