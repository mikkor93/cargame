﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class AchievementEvents : MonoBehaviour {

    void OnEnable() {
        //Subscribing to an event
        EventManager.OnSpeedReached += Achievement_OnSpeedReached;
    }

    void OnDisable() {
        //Unsubscribing from an event
        EventManager.OnSpeedReached -= Achievement_OnSpeedReached;
    }

    void Achievement_OnSpeedReached() {
        //Note: Here we give the achievement
        Debug.Log("OnSpeedReached achievement earned!");
    }
}
