﻿/*
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;
using UnityEngine.SocialPlatforms;
using System;

public class GooglePlayManager : MonoBehaviour, RealTimeMultiplayerListener {

    public enum MultiplayerGameType {
        QuickRace
    }

    public MultiplayerGameType gameType;

    Invitation inv = null;

    public Text playerName;
    public Text debugText;

    private bool showWaitingRoom = false;

    private static GooglePlayManager _instance = new GooglePlayManager();
    public static GooglePlayManager Instance {
        get {
            return _instance;
        }
    }


	void Start () {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                 // registers a callback to handle game invitations.
                 .WithInvitationDelegate(OnInvitationReceived)
                     .Build();

        PlayGamesPlatform.InitializeInstance(config);

        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();

        Login();
    }

    public void Login() {
         Social.localUser.Authenticate((bool success) => {
            if(success) {
                playerName.text = Social.localUser.userName;
            }
            else {
                 playerName.text = "Login failed";
             }
        });
    }

    public void MultiplayerButton() {
        if(Social.localUser.authenticated) {
            const int minOpponents = 1, maxOpponents = 3;
            const int gameVariant = 0;

            PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(minOpponents, maxOpponents, gameVariant, this);
            //PlayGamesPlatform.Instance.RealTime.CreateQuickGame(1, 1, 0, this);
        }
        else {
            Login();
        }
    }

    //Handle invitation
    public void OnInvitationReceived(Invitation invitation, bool autoAccept) {
        PlayGamesPlatform.Instance.RealTime.AcceptInvitation(invitation.InvitationId, this);
    }

    #region Room stuff
    public void OnRoomSetupProgress(float percent)
    {
        if (!showWaitingRoom) {
            showWaitingRoom = true;
            PlayGamesPlatform.Instance.RealTime.ShowWaitingRoomUI();
        }
    }

    public void OnRoomConnected(bool success)
    {
        debugText.text = "Room connected";
    }

    public void OnLeftRoom()
    {
    }

    public void OnParticipantLeft(Participant participant)
    {
    }

    public void OnPeersConnected(string[] participantIds)
    {
    }

    public void OnPeersDisconnected(string[] participantIds)
    {
    }

    public void OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
    }
    #endregion

}
*/