﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDatabse {

    static private List<Car> _cars;
    static private bool isDatabaseLoaded = false;

    static private void ValidateDatabase() {
        if(_cars == null)_cars = new List<Car>();
        if (!isDatabaseLoaded) LoadDatabase();
    }

    static public void LoadDatabase() {
        if (isDatabaseLoaded) return;
        isDatabaseLoaded = true;
        LoadDatabaseForce();
    }

    static public void LoadDatabaseForce() {
        ValidateDatabase();
        Car[] resources = Resources.LoadAll<Car>(@"Cars");

        foreach(Car car in resources) {
            if(!_cars.Contains(car)) {
                _cars.Add(car);
            }
        }
    }

    static public void ClearDatabase() {
        isDatabaseLoaded = false;
        _cars.Clear();
    }

    static public Car GetCar(int id) {
        ValidateDatabase();

        foreach (Car car in _cars) {
            if(car.carIndex == id) {
                return ScriptableObject.Instantiate(car) as Car;
            }
        }

        return null;
    }
}
