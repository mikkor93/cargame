﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarLoader : MonoBehaviour {

    private Transform carRoot;


	void Start () {
        //Find the car root
        carRoot = GameObject.FindWithTag("Player").transform;

        Car car = CarDatabse.GetCar(DataRepo.selectedCarID);
        GameObject mesh = Instantiate(car.mesh);
        mesh.transform.parent = carRoot.GetChild(0);
        mesh.transform.position = carRoot.position;
        mesh.transform.Rotate(0, 180, 0);

        Debug.Log("Selected car ID: " + DataRepo.selectedCarID);
	}
}
