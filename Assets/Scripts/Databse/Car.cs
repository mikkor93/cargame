﻿using UnityEngine;
using System.Collections;


public class Car : ScriptableObject {
    public int carIndex;
    public string carName;
    public float acceleration;
    public float brakes;
    public float handling;
    public float topSpeed;

    public GameObject mesh;
}
