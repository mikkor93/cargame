﻿using System.Collections;
using UnityEngine;
using UnityEditor;

public class CarCreator {
    [MenuItem("Car Game/Car Creator")]
    static public void CreateCar() {
        ScriptableObjectUtility.CreateAsset<Car>();
    }
}
